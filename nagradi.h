#pragma once

#include "igra.h"

class _nagradi
{
private:
char i;
float risuvai;
double t[10], t_radius, radius2, timeout;

public:
double maxx, maxy;

_nagradi(){risuvai=0;}
~_nagradi(){}

void nova(ship::coords *poziciq, double *Cdelay, float tip)
{
timeout = *Cdelay + 10000;
risuvai=tip;

t[0] = poziciq->x;
t[1] = poziciq->y;
t_radius = maxx * 0.015;
radius2 = t_radius * t_radius;
}

void status(float x, float y)
{
 float risuvai_staro = risuvai,
       staro_t[2]={t[0], t[1]};
 
 risuvai = 1; 
 t[0] = x;
 t[1] = y;
 t_radius = maxx * 0.015;
 
 OutTextXY(x + maxx * 0.02, y + maxy * 0.006, 0.5, font.hud,"%1.0lf%%", izkarvane_na_krv[4] / 0.0013);
 obraz();
 
 risuvai = risuvai_staro;
 t[0]=staro_t[0];
 t[1]=staro_t[1];
}

void obraz()
{
if (!risuvai) return;

float color = random(100) * 0.003;

glEnable(GL_BLEND);

if (risuvai == 1)
{
t[2] = t[0] + t_radius * cos(M_PI2);
t[3] = t[1] + t_radius * sin(M_PI2);
t[4] = t[0] + t_radius * cos(M_PI * 1.5);
t[5] = t[1] + t_radius * sin(M_PI * 1.5);

t[6] = t[0] + t_radius;
t[7] = t[1];
t[8] = t[0] + t_radius * cos(M_PI);
t[9] = t[1] + t_radius * sin(M_PI);

glColor3f(color, 0.7 + color, color);
glLineWidth(1.0);

glBegin(GL_LINES);
 line(t[2], t[3], t[4], t[5], 0, 1);
 line(t[6], t[7], t[8], t[9], 0, 1);
glEnd();
}

else if (risuvai==2)  glColor3f(color - 0.2, 0.1 + color, 0.7 + color);

 glPushMatrix();
 glTranslatef(transformX(t[0]), transformY(t[1]), 0);
 gluDisk(quad, transformX(maxx * 0.135), transformX(maxx * 0.1375), maxx * 0.035, 1);
 glPopMatrix();
}

void kontrol(double *Cdelay, ship::coords korab, double korab_radius, int *krv, ship::coords *ubit, unsigned char rajdane_lim)
{
 double d;
 
 if (ubit->posoka)
    {
      if (random( (25 - rajdane_lim / 6) * 0.705) == 1) 
         {
          nova(ubit, Cdelay, ubit->posoka);
          zvuk.timeout.nagrada_p = -2;
         }
      ubit->posoka=0;
    }

 if (risuvai)
    {
      if (timeout < *Cdelay) risuvai=0;
	  d=razstoqnie(korab.x, korab.y, t[0], t[1]);
      if (d <= korab_radius*korab_radius + radius2)
         {
           if (risuvai < 2.0)
              {
                if (izkarvane_na_krv[4] < 0.5)
                    for (i=4; i>=0; i--)
                        izkarvane_na_krv[i]*=1.1;
              }
           else *krv += 40;
           
           zvuk.timeout.nagrada_vz[0] = -2;
           risuvai=0; toczki+=50;
         }
    }
 
}

} nagrada;
