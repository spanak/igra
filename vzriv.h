#pragma once

#include "igra.h"

const unsigned maxPoints=250, maxPointsSmall=7; 

class explosion
{
private:
	double cvqt;
	unsigned int i, j, max;

	struct points
	{ship::coords koord; double teglo;} *toczki[maxPoints];

	struct _vreme { double nov, progres; };

	struct _vzriv
	{
		ship::coords koordinati;
		points toczki[maxPoints];
		float izostavane;
		unsigned short progres;
		bool siten;
	};

public:
  double radius[2];
  unsigned int maxx, maxy;
  ship::coords *koordinati;
  _vreme vreme, timeout;
  _vzriv *vzriv;

explosion(){}
explosion(const int maxExplosions)
{
  max=maxExplosions;
  vzriv = new _vzriv [max];
  for (i=0;i<max;i++) vzriv[i].progres=0;
  
  randomize();
}

~explosion(){}

void nov(ship::coords poziciq, bool siten)
{  
	for (j=0; j<max; j++)
	{
		if (vzriv[j].progres) continue;
		
		vzriv[j].izostavane = 0;
		vzriv[j].progres=1;
		vzriv[j].siten=siten;
	
		for (i=0; i<maxPoints; i++) 
		{
			// ��������� ������� �� ���� ����� �����
			if (siten && i >= maxPointsSmall)
			{
				vzriv[j].toczki[i].koord.x = -1;
				vzriv[j].toczki[i].koord.y = -1;
				continue;
			}
			vzriv[j].toczki[i].koord.x=poziciq.x;
			vzriv[j].toczki[i].koord.y=poziciq.y;
			vzriv[j].toczki[i].koord.posoka = random(314159265)/100000000.0;
			if (random(2)) vzriv[j].toczki[i].koord.posoka = -vzriv[j].toczki[i].koord.posoka;

			if (i<5*maxPoints/7)
				vzriv[j].toczki[i].teglo=random(300)/300.0;
			else
				vzriv[j].toczki[i].teglo=0.6 + random(1000)/7000.0;
		}
	 
		if (!siten) 
		{
			zvuk.timeout.vzriv = -2;
			zvuk.vzriv_x = (unsigned)poziciq.x;
		}
		else
		{
			zvuk.timeout.ucelen = -2;
			zvuk.ucelen_x = (unsigned)poziciq.x;
		}
		
		break;
	}
}

void razpilqvane(double Cdelay)
{
	if (Cdelay - this->vreme.progres < this->timeout.progres)
		return;

	unsigned broy_toczki;
	float stpka_toczka;
		
	for (this->j=0; this->j < this->max; this->j++)
		this->vzriv[j].izostavane = (Cdelay - this->vreme.progres) / this->timeout.progres;

	for (this->j=0; this->j < this->max; this->j++)
	{
		if (!this->vzriv[j].progres)
			continue;   
		
		if (this->vzriv[j].progres++ > 35)
			this->vzriv[j].progres=0;

		broy_toczki = (this->vzriv[j].siten) ? maxPointsSmall : maxPoints;
		
		for (i=0; i < broy_toczki; i++)
		{
			stpka_toczka = 
					radius[this->vzriv[j].siten]
					* this->vzriv[j].toczki[i].teglo
					* (1.5 - this->vzriv[j].progres / 90);

			this->vzriv[j].toczki[i].koord.x += stpka_toczka * cos(this->vzriv[j].toczki[i].koord.posoka);
			this->vzriv[j].toczki[i].koord.y += stpka_toczka * sin(this->vzriv[j].toczki[i].koord.posoka);
		}
		
		// ��� ����������, ������ ��� ���� ������ � ���������� �� ������
		if (this->vzriv[j].izostavane > 1.07)
		{
			this->vzriv[j].izostavane -= 1;
			j--;
		}
	}
	
	vreme.progres = Cdelay;
}

void obraz()
{ 
for (j=0;j<max;j++)
 {
  if (!this->vzriv[j].progres) continue;   
  cvqt=(log(36.995 - this->vzriv[j].progres)-0.66)/2.9;
  glEnable(GL_BLEND);
  glDisable(GL_POINT_SMOOTH);
  glPointSize(1);
  glColor4f(1, 1, 0.35, cvqt);
  glBegin(GL_POINTS);
  
  for (i=0;i<maxPoints;i++)
      {
      glVertex3f(transformX((this->vzriv[j].toczki[i].koord.x-2)), transformY(this->vzriv[j].toczki[i].koord.y), 0.3);
      glVertex3f(transformX((this->vzriv[j].toczki[i].koord.x)), transformY((this->vzriv[j].toczki[i].koord.y-2)), 0.3);
      glVertex3f(transformX((this->vzriv[j].toczki[i].koord.x)), transformY((this->vzriv[j].toczki[i].koord.y+2)), 0.3);
      glVertex3f(transformX((this->vzriv[j].toczki[i].koord.x+2)), transformY(this->vzriv[j].toczki[i].koord.y), 0.3);
      }
  glEnd();
  }
}

};