#pragma once

#include <windows.h>
#include <mmsystem.h>

void randomize()
{ srand( (unsigned)time( NULL ) ); }

int random(unsigned int number)
{ 
 double retval = ((double) rand() / (double) RAND_MAX) * number; 
 
 return (int) retval;
}

class midi
{
private:
   int midiport;
   int greszka;
   HMIDIOUT ustrojstvo;
   struct coords { double x, y, posoka; };
   
   typedef union { unsigned long word; unsigned char byte[4]; } midi_info;
   midi_info info;
   
public:
 __int8 zvuk;
 unsigned maxx, vzriv_x, ucelen_x;
 
 struct vremena 
 {double kurszum_zlo, kurszum, vzriv, fon[5], gorivo, ucelen,
         nagrada_p, nagrada_vz[2], jivot;} timeout; 
  
 midi()
 {
//     timeout.proba = -1;
     timeout.kurszum_zlo = -1;
     timeout.kurszum = -1;
     timeout.vzriv = -1;
     timeout.fon[0] = 0;
     timeout.fon[1] = 2750;
     timeout.fon[2] = 5250;
     timeout.fon[3] = 7550;
     timeout.fon[4] = 10000;
     timeout.gorivo = -1;
     timeout.nagrada_p = -1;
     timeout.nagrada_vz[0] = -1;
     timeout.nagrada_vz[1] = -1;
     timeout.jivot = -1;
     
     midiport = 0;
     zvuk = 127;
     
     if ( !midiOutGetNumDevs() ) zvuk = -1;
     else
         {
          greszka = midiOutOpen(&ustrojstvo, midiport, 0, 0, CALLBACK_NULL);
          if (greszka != MMSYSERR_NOERROR) zvuk = -1;
         }
  }
   
   ~midi()
    {
      if (zvuk >= 0)
         {
          midiOutReset(ustrojstvo);
          midiOutClose(ustrojstvo);
         }
    }
 void stop( void )
 {
   midiOutReset(ustrojstvo);
 }

 void pusni( __int8 instrument, __int8 visoczina, __int8 kanal, __int8 balans, __int8 sila)
 {
  if (zvuk < 0) return;
  
  if (kanal < 0) kanal = 0;
  if (kanal > 15) kanal = 15;
  if (visoczina < 0) visoczina = 0;
  
  info.byte[3] = 0;
  
  if (balans >= 0)
     {
      info.byte[0] = 0xb0 + kanal; // continuous controller
      info.byte[1] = 10;           // panning
      info.byte[2] = balans;       // position
      midiOutShortMsg(ustrojstvo, info.word);
     }
  
  if (sila >= 0)
     {
      info.byte[0] = 0xb0 + kanal; // continuous controller
      info.byte[1] = 7;            // volume
      info.byte[2] = sila;
      midiOutShortMsg(ustrojstvo, info.word);
     }
  
  if (instrument >= 0)
     {
      info.byte[0] = 0xc0 + kanal;  // ����� �� ����������
      info.byte[1] = instrument;    // ����� �� �����������
      info.byte[2] = 0;
      midiOutShortMsg(ustrojstvo, info.word);
     }
     
  info.byte[0] = 0x90 + kanal; //����� ������
  info.byte[1] = visoczina;    //�������� �� ����, 60 - ������ ��
  info.byte[2] = 100;          //velocity - � ���� �� ��������
  midiOutShortMsg(ustrojstvo, info.word); 
 }
 
 void spri(__int8 kanal, __int8 visoczina)
 {
  if (zvuk < 0) return;
  
  info.byte[0] = 0x90 + kanal;
  info.byte[1] = visoczina;
  info.byte[2] = 0;
  info.byte[3] = 0;
  midiOutShortMsg(ustrojstvo, info.word);
 }
 
 void vzriv(double Cdelay)
 {
 if ((!zvuk) || (timeout.vzriv == -1)) return;
  
  if (timeout.vzriv == -2)
     {
      char balans;
  
      balans = (char)(127 * vzriv_x / maxx);
      pusni(56, 75, 9, balans, zvuk);
        
      timeout.vzriv = Cdelay + 450;
     }
   
   else
       if (timeout.vzriv <= Cdelay) 
          {
           spri(9, 75);
           timeout.vzriv = -1;
          }
 }
 
 void kurszum(double Cdelay, double poziciq)
 {
 if ((!zvuk) || (timeout.kurszum == -1)) return;
  
  if (timeout.kurszum == -2)
     {
      char balans;
  
      balans = (char)(127 * poziciq / maxx);
      pusni(127, (char)(59+random(3)), 0, balans, zvuk/2);
        
      timeout.kurszum = Cdelay + 250;
     }
   
   else
       if (timeout.kurszum <= Cdelay) 
          {
           spri(0,61);
           spri(0,60);
           spri(0,59);
           timeout.kurszum = -1;
          }
 }
 
 void kurszum_zlo(double Cdelay)
 {
 if ((!zvuk) || (timeout.kurszum_zlo == -1)) return;
  
  if (timeout.kurszum_zlo == -2)
     {  
      pusni(23, 45, 1, 64, (char)(zvuk/2.25));
      timeout.kurszum_zlo = Cdelay + 100;
     }
   
   else
       if (timeout.kurszum_zlo <= Cdelay) 
          {
           spri(1, 45);
           timeout.kurszum_zlo = -1;
          }
 }

 void kosmos(double Cdelay)
 {
 if (!zvuk) return;
 
 if (Cdelay >= timeout.fon[0])
    { 
      spri(2, 30);
      pusni(122, 30, 2, (char)random(128), zvuk/5);
      timeout.fon[0] = Cdelay + 3000 + random(500);
    }
 
 if (Cdelay >= timeout.fon[1])
    { 
      spri(2, 20);
      pusni(122, 20, 3, (char)random(128) & 0xFF, zvuk/5);
      timeout.fon[1] = Cdelay + 3000 + random(500);
    }
 
 if (Cdelay >= timeout.fon[2])
    { 
      spri(2, 15);
      pusni(122, 15, 4, (char)random(128) & 0xFF, zvuk/4);
      timeout.fon[2] = Cdelay + 3000 + random(500);
    }
 if (Cdelay >= timeout.fon[3])
    { 
      spri(2, 10);
      pusni(122, 10, 5, (char)random(128) & 0xFF, zvuk/4);
      timeout.fon[3] = Cdelay + 3000 + random(500);
    }
 if (Cdelay >= timeout.fon[4])
    { 
      spri(2, 5);
      pusni(122, 5, 6, (char)random(128) & 0xFF, zvuk/4);
      timeout.fon[4] = Cdelay + 3000 + random(500);
    }
    
 }

 void gorivo(double Cdelay, double poziciq)
 {
  if ((!zvuk) || (timeout.gorivo == -1)) return;
  
  if (timeout.gorivo == -2)
     {
      char balans, vol = (char) zvuk / 2;
  
      balans = (char)(127 * poziciq / maxx);
      pusni(85, 30, 7, balans, vol);
      pusni(85, 10, 7, balans, vol);
      pusni(85, 3, 7, balans, vol);
        
      timeout.gorivo = Cdelay + 50;
     }
   
   else
       if (timeout.gorivo <= Cdelay) 
          {
           spri(7, 30);
           spri(7, 10);
           spri(7, 3);
           timeout.gorivo = -1;
          }
 }
 void ucelen(double Cdelay)
 {
 if ((!zvuk) || (timeout.ucelen == -1)) return;
  
  if (timeout.ucelen == -2)
     {
      char balans;
  
      balans = (char)(127 * ucelen_x / maxx);
      pusni(113, 60, 10, balans, zvuk/3);
      pusni(98, 50, 8, balans, zvuk/3);
        
      timeout.ucelen = Cdelay + 50;
     }
   
   else
       if (timeout.ucelen <= Cdelay) 
          {
           spri(10, 60);
           spri(8, 50);
           timeout.ucelen = -1;
          }
  }
  
  void nagrada_poqvqvane(double Cdelay)
  {
  if ((!zvuk) || (timeout.nagrada_p == -1)) return;
  
  if (timeout.nagrada_p == -2)
     {
      pusni(96, 72, 11, 63, (char)(zvuk/1.75));
      timeout.nagrada_p = Cdelay + 100;
     }
   
   else
       if (timeout.nagrada_p <= Cdelay) 
          {
           spri(11, 72);
           timeout.nagrada_p = -1;
          }
  }

  void nagrada_vzeta(double Cdelay)
  {
  if ((!zvuk) || ((timeout.nagrada_vz[0] == -1) && (timeout.nagrada_vz[1] == -1))) return;
  
  if (timeout.nagrada_vz[0] == -2)
     {
      pusni(90, 60, 11, 63, zvuk/2);
      timeout.nagrada_vz[0] = Cdelay + 100;
     }
   
   else
       if ((timeout.nagrada_vz[0] <= Cdelay) && (timeout.nagrada_vz[1] == -1)) 
          {
           spri(11, 60);
           timeout.nagrada_vz[0] = -3;
           pusni(90, 67, 11, 63, zvuk/2);
           timeout.nagrada_vz[1] = Cdelay + 500;
          }
   
   if ((timeout.nagrada_vz[1] <= Cdelay) && (timeout.nagrada_vz[0] == -3) )
      {
        spri(11, 67);
        timeout.nagrada_vz[1] = -1;
        timeout.nagrada_vz[0] = -1;
      }
  }

 void jivot(double Cdelay)
 {
if ((!zvuk) || (timeout.jivot == -1)) return;
  
  if (timeout.jivot == -2)
     {
      pusni(55, 60, 12, 63, zvuk/2);
      pusni(91, 84, 13, 63, zvuk/2);
      timeout.jivot = Cdelay + 250;
     }
   
   else
       if (timeout.jivot <= Cdelay) 
          {
           spri(12, 60);
           spri(13, 84);
           timeout.jivot = -1;
          }
 }

 void kontrol(double Cdelay, double x)
 {
  kurszum(Cdelay, x);
  kurszum_zlo(Cdelay);
  vzriv(Cdelay);
  kosmos(Cdelay);
  gorivo(Cdelay, x);
  ucelen(Cdelay);
  nagrada_poqvqvane(Cdelay);
  nagrada_vzeta(Cdelay);
  jivot(Cdelay);
 }

} zvuk;