#define _WIN32_WINNT 0x0500

#include "gadini.h"
#include "nagradi.h"

bool status=1, entelekt=1, peczelisz=1, limitFPS;
int maxBullets=9, maxExplosions=50, maxEnemies=120, i, CurrentFrame=0;
double Cdelay, vremeFPS, timeout, vremeFn, timeoutFn,
       FPS=65, vremeScena=0, frames, vremeZvuk=0;
ship::coords nov_vzriv[20], nova_strelba={0,0,200},
             status_koords[2];
ship korab;
bullet strelba(maxBullets, maxEnemies);
explosion vzriv(maxExplosions);
__gadini gadini(maxEnemies);


//void mitrele();

void CalculateTimeouts(double timeout)
{
	korab.timeout.napred=timeout / 275;
	korab.timeout.zavoi=timeout / 250;
	strelba.timeout.napred = timeout / 200;
	strelba.timeout.nova = timeout / 29;
	vzriv.timeout.nov = timeout / 2;
	vzriv.timeout.progres = timeout / 160;
	
	gadini.timeout_rajdane = timeout / 30;
	gadini.timeout_rajdane_poziciq = 250 + random(250);
	gadini.timeout_strelba = timeout * 1.5;
	
	gadini.timeout_napred[0] = timeout / 275;
	gadini.timeout_smqna_na_posoka[i] = random(timeout*2);
	gadini.timeout_zavoi[0] = timeout / 245;
	
	gadini.timeout_napred[1] = timeout / 185;
	gadini.timeout_smqna_na_posoka[i] = random(timeout*2);
	gadini.timeout_zavoi[1] = timeout / 145;
	
	gadini.timeout_napred[2] = timeout / 235;
	gadini.timeout_smqna_na_posoka[i] = random(timeout*2);
	gadini.timeout_zavoi[2] = timeout / 235;
	
	gadini.timeout_napred[3] = timeout / 235;
	gadini.timeout_smqna_na_posoka[i] = random(timeout*2);
	gadini.timeout_zavoi[3] = timeout / 200;
	
	gadini.timeout_napred[4] = timeout / 185;
	gadini.timeout_smqna_na_posoka[i] = random(timeout*2);
	gadini.timeout_zavoi[4] = timeout / 145;
    
}

void DefaultSettings(bool restart, bool golqm_restart)
{
if (restart) goto res;
if (golqm_restart) goto golqm_res;

  ezik=0;
  for (i=0; i<20; i++) nov_vzriv[i].posoka=0;

  Cdelay=clock();
  vremeFn=Cdelay;
  vremeFPS=Cdelay;
  korab.vreme.napred=Cdelay;
  korab.vreme.zavoi=Cdelay;
  strelba.vreme.napred=Cdelay;
  strelba.vreme.nova=Cdelay;
  vzriv.vreme.nov=Cdelay;
  vzriv.vreme.progres=Cdelay;
  gadini.vreme_rajdane=Cdelay;
  gadini.vreme_rajdane_poziciq=Cdelay;
  for (i=0;i<maxEnemies;i++)
      {
       gadini.gad[i].vreme.napred=Cdelay;
       gadini.gad[i].vreme.smqna_na_posoka=Cdelay;
       gadini.gad[i].vreme.zavoi=Cdelay;
       gadini.gad[i].vreme.strelba_nova=Cdelay;
      }

  timeoutFn = 50;
  CalculateTimeouts(2664); //������� �� ������; �������� - 2664
  
  DisplayParameters.x = 800;
  DisplayParameters.y = 600;
  DisplayParameters.BitsPerPixel = 32;
  DisplayParameters.RefreshRate  = 60;
  DisplayParameters.FullScreen = 0;

golqm_res:
  maxx = DisplayParameters.x;
  maxy = DisplayParameters.y;

 status_koords[0].x = maxy / 40;
 status_koords[0].y = maxx / 20;
 status_koords[0].posoka = M_PI2;
 
 status_koords[1].x = maxy / 40;
 status_koords[1].y = maxy / 40;
 status_koords[1].posoka = M_PI2;

 triygylnik();
 
res:
korab.koordinati.x=maxx / 2; //������� ������� � ������ �� ���������
korab.koordinati.y=maxy / 2;
korab.koordinati.posoka = M_PI2;
korab.razmer=maxy*0.65;
korab.radius=korab.razmer / 25.0;
korab.maxx=maxx;
korab.maxy=maxy;
if (!golqm_restart) korab.jivoti=3;

if (restart)
   {
     otbroqvane=4;
     izkarvane_normalno();
     for (i=0; i<maxEnemies; i++) gadini.gad[i].krv=0;
     for (i=0; i<maxBullets+maxEnemies; i++) strelba.koordinati[i].posoka=200;
     kop.spri_proverka=0;
     limitFPS=1;
     if (testova_igra) {nivo=200;kop.spri_proverka=1;limitFPS=0;}
     korab.krv=100;
     vremeScena=0;
     peczelisz=1;
     gadini.init(maxEnemies); 
     return;
   }

strelba.maxx=maxx;
strelba.maxy=maxy;
strelba.radius=DisplayParameters.y/120.0;

vzriv.maxx=maxx;
vzriv.maxy=maxy;
vzriv.radius[0] = maxx / 975;
vzriv.radius[1] = vzriv.radius[0] * 0.5;

gadini.maxy=maxy;
gadini.maxx=maxx;
gadini.razmer[0]=maxy*0.5;
gadini.radius[0]=gadini.razmer[0]/19.0;
gadini.razmer[1]=maxy*0.65;
gadini.radius[1]=gadini.razmer[1]/20.0;
gadini.razmer[2]=maxy*0.65;
gadini.radius[2]=gadini.razmer[2]/20.0;
gadini.razmer[3]=maxy*0.75;
gadini.radius[3]=gadini.razmer[3]/22.0;
gadini.razmer[4]=maxy*0.85;
gadini.radius[4]=gadini.razmer[4]/21.0;

zvezdi.init(DisplayParameters.x, DisplayParameters.y);

nagrada.maxx = maxx;
nagrada.maxy = maxy;

zvuk.maxx = maxx;
}

void DisplayStatus()
{
  gadini.push(status_koords[0]);
  korab.push(status_koords[1]);
  glDisable(GL_BLEND);
  glDisable(GL_LINE_SMOOTH);
  glLineWidth(1);

  gadini.obraz2(0);
  korab.obraz1();
  
  gadini.pop();
  korab.pop();
  
  glColor3f( 0.7, 0.7, 0.7 );

  OutTextXY(maxy * 0.05, maxx * 0.025, 0.5, font.hud,"x %d; %d%%",korab.jivoti, korab.krv);
  OutTextXY(maxy * 0.05, maxy * 0.076, 0.5, font.hud,"x %d",gadini.jivi);
  OutTextXY(maxy * 1.22, maxy * 0.02, 0.5, font.hud,"FPS: %1.0f",FPS);
  OutTextXY(maxy * 0.65, maxy * 0.025, 0.5, font.hud,"[ %d ] [ %1.0lf ]", nivo, toczki);
  
  if (vremeZvuk >= Cdelay)
     {
      glColor3f(1,1,0.3);
      OutTextXY(maxx * 0.45, maxy * 0.98, 0.5, font.hud,"F7 - (( %d%% )) + F8", 100*zvuk.zvuk/127);
     }
  
  glColor3f( 0.7, 0.7, 0.7 );
  nagrada.status(maxy * 0.026, maxy * 0.114);
}

void __mainLoop()
{
Cdelay=clock();

kop.proverka();
if (!korab.jivoti)
   {
     kop.spri_proverka=1;
     peczelisz=0;
     if (!vremeScena) vremeScena=Cdelay+1000;
   }

strelba.kontrol(Cdelay, korab.nos);
strelba.polet(Cdelay);
gadini.kontrol(korab.koordinati, korab.radius, strelba.koordinati, nov_vzriv, maxBullets);
gadini.butane(&korab.koordinati, korab.radius);
gadini.entelekt(Cdelay, &nova_strelba);
strelba.nova(&nova_strelba, 1);
korab.smrt(strelba.koordinati, nov_vzriv, maxBullets, maxEnemies);
korab.polet(Cdelay);
vzriv.razpilqvane(Cdelay);
nagrada.kontrol(&Cdelay, korab.koordinati, korab.radius, &korab.krv, &gadini.ubit, gadini.rajdane_lim);
zvuk.kontrol(Cdelay, korab.koordinati.x);

if (!gadini.jivi && nivo <= 201 && !otbroqvane) 
   {
     nivo++;
     if (nivo <= 201)
        {
          gadini.rajdane=1; gadini.rodeni=0;
          otbroqvane=4;
        }
     else             
        {
          vremeScena=Cdelay+1500;
          peczelisz=1;
        }
   }

if (otbroqvane && vremeOtbroqvane < Cdelay)
   {
     vremeOtbroqvane = Cdelay + 1000;
     otbroqvane--;
   }

if (gadini.dai_jivot < toczki)
   {
     gadini.dai_jivot += 7500.0;
     korab.jivoti++;
     zvuk.timeout.jivot = -2;
   }

if (gadini.rajdane && !otbroqvane) gadini.RodiGadini(Cdelay, 0);

if (vremeScena <= Cdelay && vremeScena) vremeScena=-1;

for (i=0; i<20; i++)
    {
     if (nov_vzriv[i].posoka) 
        {
          if (nov_vzriv[i].posoka > -M_2PI) vzriv.nov(nov_vzriv[i], 0);
          else                              vzriv.nov(nov_vzriv[i], 1);
   
          nov_vzriv[i].x+=7.5*cos(nov_vzriv[i].posoka);
          nov_vzriv[i].y+=7.5*sin(nov_vzriv[i].posoka);
   
          if (nov_vzriv[i].posoka > -M_2PI) vzriv.nov(nov_vzriv[i], 0);
          else                              vzriv.nov(nov_vzriv[i], 1);
     
          nov_vzriv[i].posoka=0;
         }
     }
    if (Cdelay - vremeFPS >= 500)
        {
          FPS = frames * ((Cdelay - vremeFPS) * 0.004);
          vremeFPS=Cdelay;
          frames=0;
        }
     else frames++;

if (kop.esc) 
   {
     izbrani_min=0;
     zvuk.stop();
     glutIdleFunc(menu_kontrol);
     glutDisplayFunc( menu );
   }

    vremeFn=Cdelay;
    if (kop.F2 && star_kop!='2') {star_kop = '2';gadini.bez_entelekt = !gadini.bez_entelekt;}
    if (!kop.F2 && star_kop=='2') star_kop = 0;
//    if (kop.F5) gadini.RodiGadini(Cdelay,1);
    if (kop.F11 && star_kop!='1') {star_kop = '1';status=!status;}
    if (!kop.F11 && star_kop=='1') star_kop = 0;
    
    if (kop.F9 && star_kop!='9') {star_kop = '9';limitFPS=!limitFPS;}
    if (!kop.F9 && star_kop=='9') star_kop = 0;
    
    if (kop.F7 && star_kop!='7') 
       {
        star_kop = '7'; if (zvuk.zvuk >= 10) zvuk.zvuk -= 10;
                        else zvuk.zvuk = 0;
                        zvuk.stop();
                        vremeZvuk = Cdelay + 2000;
       }
    if (!kop.F7 && star_kop=='7') star_kop = 0;
    
    if (kop.F8 && star_kop!='8') 
       {
        star_kop = '8'; 
        if (zvuk.zvuk <= 117) zvuk.zvuk += 10;
        else zvuk.zvuk = 127;
        zvuk.stop();
        vremeZvuk = Cdelay + 2000;
       }
    if (!kop.F8 && star_kop=='8') star_kop = 0;
    
   
 if (limitFPS)
    if (Cdelay == clock()) Sleep(1); //�� �� �� �������� 100% ���������
}

void kosmos(void)
{
	glutSwapBuffers();

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	if (korab.jivoti) korab.obraz1();
	strelba.obraz();
	vzriv.obraz();
	gadini.obraz();
	zvezdi.obraz();
	nagrada.obraz();
	if (status) 
		DisplayStatus();
		
	if (otbroqvane) 
	{
		glColor3f(1,1,1);
		OutTextXY(DisplayParameters.x * 0.49, DisplayParameters.y * 0.45, 0.9, font.menu, "%d", otbroqvane);
	}
	if (vremeScena < 0) 
	{
		glColor3f(1,1,0.5);
		OutTextXY(DisplayParameters.x * 0.37, DisplayParameters.x * 0.36, 0.9, font.menu, msg_krai_na_igrata[ezik]);
		if (peczelisz)
			OutTextXY(DisplayParameters.x * 0.37, DisplayParameters.x * 0.41, 0.9, font.menu, msg_peczelisz[ezik]);
	}

	//glFlush();
	glutPostRedisplay();
}

int main(int argc, char** argv)
{
	HWND window = GetConsoleWindow();

	kop.DefaultKeys();
	cfg.load();
	DefaultSettings(0,0);
	CFGSettings();
	CharacterSet = (ezik < 2) ? RUSSIAN_CHARSET : ANSI_CHARSET;
	
	ShowWindow(window, SW_HIDE);

	glutInit(&argc,argv);

	initgraph(zaglavie);
	gluQuadricDrawStyle(quad, GLU_SILHOUETTE);
	font.hud  = BuildFont(zaglavie, "Arial", DisplayParameters.x / 60, FW_BOLD, 0, 0, 0);
	font.menu = BuildFont(zaglavie, "Arial", DisplayParameters.y / 20, FW_BOLD, 0, 0, 0);

	//������ �����
	glutKeyboardFunc( NULL );
	glutSpecialFunc( NULL );
	glutReshapeFunc( resizeWindow );
	glutDisplayFunc( menu );
	glutIdleFunc(menu_kontrol);
	glutMainLoop();

	return 0;
}
