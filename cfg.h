#pragma once
#pragma warning(disable:4996)

#include <stdio.h>
#include <string.h>

#include "zvuk.h"

unsigned long CharacterSet=204; //Russian

class CFG
{
private:
 char *config, path[256];
 
public:

CFG()
{
  config=NULL; 
  strcpy(path, ".\\igra.cfg");
}

~CFG(){}

void write()
{
  size_t i=0, lim;
  
  if (config==NULL) return;

  FILE *cfg;

  cfg=fopen(path,"w");  
  if (cfg == NULL) return;
  
  fprintf(cfg, ";;;; igra701a - ���� � ��������� ;;;;\n\n");
  
  i=0; lim = strlen(config);
  while (config[i]=='\n') i++;
  
  while(i<lim) 
      {
       if ((i>0) && (config[i]=='\n') && (config[i-1]=='\n')) {i++; continue;}
       if ((i == lim - 1) && (config[i]=='\n')) break;
       
       fputc(config[i], cfg);
       i++;
      }
      
  fclose(cfg);
}

void load()
{
FILE   *cfg;
size_t lim=0, i;

cfg = fopen(path, "r");
if (cfg == NULL) {config=NULL; return;}

while ( !feof(cfg) ) {lim++; fgetc(cfg);}
config = new char[lim + 1];

fseek(cfg, SEEK_CUR-1, SEEK_SET);
for (i=0; (i<lim) && !feof(cfg); i++) config[i] = (char)fgetc(cfg);
config[i-1]=0;

fclose(cfg);
}

char *value(char *variable)
{
if (config==NULL) return "-1";

size_t i;
char  *s;
char static retval[64];
 
 s  = strstr(config, variable);
 if (s == NULL) return "-1";
 
 s += strlen(variable);
 
 i=0;
 while ( (s[i] != '\n') && (i<strlen(s)) ) retval[i]=s[i++];
 retval[i]=0;

return retval;
}

void change(char *variable, char *new_value)
{
size_t length, pos;
char  *s, 
      *backup;
 
 if (config == NULL) goto add_value;
 s  = strstr(config, variable);
 
 if (s == NULL) 
    {
add_value:
      backup = new char [strlen(variable) + strlen(new_value) + 2];
      strcpy(backup, "\n");
      strcat(backup, variable);
      strcat(backup, new_value);
      if (config != NULL)
         {
           s = new char [strlen(config) + 1];
           strcpy(s, config);
           delete [] config;
           config = new char [strlen(s) + strlen(backup) + 1];
           strcpy(config, s);
           strcat(config, backup);
           delete [] s;
         }
      else
         {
           config = new char [strlen(backup) + 1];
           strcpy(config, backup);
         }
    }
 
 else
    { 
      size_t tmp = strlen(new_value) - strlen(value(variable));
      if (tmp > 0)
         {
          backup = new char [strlen(config) + 1];
          strcpy(backup, config);
          delete [] config;  
          config = new char [strlen(backup) + tmp + 1];
          strcpy(config, backup);
          delete [] backup;
          s = strstr(config, variable);
         }
 
      backup = new char [strlen(config) + 1];
      length = strlen(config) - strlen(s);
      
      s += strlen(variable);

      for (pos=0; pos<strlen(s); pos++) 
          if ( s[pos]=='\n' ) 
               break;

      s += pos;
      strcpy(backup, s);
      s -= (pos + strlen(variable));
      s[strlen(variable)] = 0;
 
      strcat(s, new_value);
      strcat(s, backup);
 
      s -= length;
      strcpy(config, s);
    }
 
 delete [] backup;
}

void rebuild(int razdelitelna, int cvetove, int czestota, bool cqlEkran, char zvuk,
             unsigned char ezik, short W, short A, short D, short TAB,
             float ludi[3], char ludi_imena[3][9])
{
 int i;
 char _new[64], lud[16];
 
 if (config != NULL) config[0]=0;
 
 sprintf(_new, "%d", razdelitelna);
 change("razdelitelna=", _new);
               
 sprintf(_new,"%d", cvetove);
 change("cvetove=",_new);
               
 sprintf(_new,"%d", czestota);
 change("czestota=",_new);
               
 sprintf(_new,"%d", cqlEkran);
 change("cqlEkran=",_new);

 if (zvuk >= 0) {sprintf(_new, "%d", zvuk);
                 cfg.change("zvuk=",_new);
                }
               
 sprintf(_new,"%d", ezik);
 change("ezik=",_new);
               
 sprintf(_new,"%d", W);
 change("W=",_new);

 sprintf(_new,"%d", A);
 change("A=",_new);

 sprintf(_new,"%d", D);
 change("D=",_new);

 sprintf(_new,"%d", TAB);
 change("TAB=",_new);
 
 for (i=0; i<3; i++)
     {
      sprintf(_new, "%s", ludi_imena[i]);
      sprintf(lud, "lud%d=", i+1); 
      change(lud, _new);
 
      sprintf(_new, "%1.0f", ludi[i]);
      sprintf(lud, "lut%d=", i+1);
      change(lud, _new);
     }
}

} cfg;