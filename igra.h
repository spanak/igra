#pragma once

#include "menu.h"

#define razstoqnie(x1,y1,x2,y2) (x1-x2)*(x1-x2) + (y1-y2)*(y1-y2)

float izkarvane_na_krv[6]={0.51, 0.26, 0.34, 0.16, 0.13, 10.0}, vremeOtbroqvane=0;
int otbroqvane=0;

void izkarvane_normalno()
{
 izkarvane_na_krv[0]=0.51;
 izkarvane_na_krv[1]=0.26;
 izkarvane_na_krv[2]=0.34;
 izkarvane_na_krv[3]=0.16;
 izkarvane_na_krv[4]=0.13;
}

class ship
{
private:
 bool pushed;
 struct _vreme { double napred, zavoi; };

public:
 double radius, razmer;
 int krv;
 unsigned maxx, maxy;
 int jivoti;
 struct coords { double x, y, posoka; };

private:
 coords stari_koordinati; 
 
public:
 coords koordinati, nos;
 _vreme vreme, timeout;

ship()
{ krv=100; pushed=0;}

~ship(){}

void push(ship::coords novi)
{ stari_koordinati=koordinati; koordinati=novi; razmer*=0.5; pushed=1;}

void pop()
{ koordinati=stari_koordinati; razmer*=2.0; pushed=0;}

void obraz1()
{
/*
 glColor3f( 0.5, 0.5, 0.5 );
 glDisable(GL_POINT_SMOOTH);
 glDisable(GL_LINE_SMOOTH);
 glDisable(GL_BLEND);
 glLineWidth(1);

 rectangle(koordinati.x-radius, koordinati.y-radius, koordinati.x+radius, koordinati.y+radius, -0.5 ,0);
 line(koordinati.x+radius,koordinati.y,koordinati.x-radius,koordinati.y,-0.5,0);
 line(koordinati.x,koordinati.y+radius,koordinati.x,koordinati.y-radius,-0.5,0);
 circle(koordinati.x, koordinati.y, 0, radius, 0);
  */
  double t[28];
  glEnable(GL_LINE_SMOOTH);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_BLEND);
  glLineWidth(1);
  
  //����� ����
  t[0]=koordinati.x + (razmer/20.0)*cos(koordinati.posoka);
  t[1]=koordinati.y + (razmer/20.0)*sin(-koordinati.posoka);
  t[2]=koordinati.x + (razmer/65.0)*cos(koordinati.posoka+M_PI*0.2);
  t[3]=koordinati.y + (razmer/65.0)*sin(-koordinati.posoka-M_PI*0.2);
  t[4]=koordinati.x + (razmer/65.0)*cos(koordinati.posoka+M_PI*1.8);
  t[5]=koordinati.y + (razmer/65.0)*sin(-koordinati.posoka-M_PI*1.8);
  //������ ����
  t[6]=koordinati.x + (razmer/60.0)*cos(koordinati.posoka+M_PI*0.5);
  t[7]=koordinati.y + (razmer/60.0)*sin(-koordinati.posoka-M_PI*0.5);
  t[8]=koordinati.x + (razmer/60.0)*cos(koordinati.posoka+M_PI*1.5);
  t[9]=koordinati.y + (razmer/60.0)*sin(-koordinati.posoka-M_PI*1.5);

  t[10]=koordinati.x + (razmer/30.0)*cos(koordinati.posoka+M_PI*0.625);
  t[11]=koordinati.y + (razmer/30.0)*sin(-koordinati.posoka-M_PI*0.625);
  t[12]=koordinati.x + (razmer/30.0)*cos(koordinati.posoka+M_PI*1.375);
  t[13]=koordinati.y + (razmer/30.0)*sin(-koordinati.posoka-M_PI*1.375);

  //����� ���� 
  t[14]=koordinati.x + (razmer/27.0)*cos(koordinati.posoka+M_PI*0.62);
  t[15]=koordinati.y + (razmer/27.0)*sin(-koordinati.posoka-M_PI*0.62);
  t[16]=koordinati.x + (razmer/27.0)*cos(koordinati.posoka+M_PI*1.38);
  t[17]=koordinati.y + (razmer/27.0)*sin(-koordinati.posoka-M_PI*1.38);

  //�����
  t[18]=koordinati.x + (razmer/20.0)*cos(koordinati.posoka+M_PI*0.74);
  t[19]=koordinati.y + (razmer/20.0)*sin(-koordinati.posoka-M_PI*0.74);
  t[20]=koordinati.x + (razmer/20.0)*cos(koordinati.posoka+M_PI*1.26);
  t[21]=koordinati.y + (razmer/20.0)*sin(-koordinati.posoka-M_PI*1.26);

  t[22]=koordinati.x + (razmer/27.0)*cos(koordinati.posoka+M_PI*0.43);
  t[23]=koordinati.y + (razmer/27.0)*sin(-koordinati.posoka-M_PI*0.43);
  t[24]=koordinati.x + (razmer/27.0)*cos(koordinati.posoka+M_PI*1.57);
  t[25]=koordinati.y + (razmer/27.0)*sin(-koordinati.posoka-M_PI*1.57);
  
  t[26]=koordinati.x + (razmer/350.0)*cos(koordinati.posoka+M_PI);
  t[27]=koordinati.y + (razmer/350.0)*sin(-koordinati.posoka-M_PI);
 
 if (!pushed)
    { 
     nos.x = t[0];
     nos.y = t[1];
     nos.posoka = -koordinati.posoka; 
    }
 
 if (krv <= 100) glColor3f(0.8 - 0.008*krv, 0.5 + 0.002*krv, 0);
 else            glColor3f(0, 0.7, 0);
 glBegin(GL_TRIANGLE_STRIP); 
 //����� ����
 glVertex3f(transformX(t[0]), transformY(t[1]), 0.2);
 glVertex3f(transformX(t[2]), transformY(t[3]), 0.2);
 glVertex3f(transformX(t[4]), transformY(t[5]), 0.2);
  
 //������ ����
 glVertex3f(transformX(t[6]), transformY(t[7]), 0.2);
 glVertex3f(transformX(t[8]), transformY(t[9]), 0.2);
 glVertex3f(transformX(t[10]), transformY(t[11]), 0.2);
 
 //����� ����
 glVertex3f(transformX(t[12]), transformY(t[13]), 0.2); 
 glVertex3f(transformX(t[14]), transformY(t[15]), 0.2);
 glVertex3f(transformX(t[16]), transformY(t[17]), 0.2); 
 glEnd();
  
 glLineWidth(2);
 glBegin(GL_LINES);
 //����� �� ��������� �� ������� :)
 line(t[16],t[17],t[14],t[15], 0, 0.2);
 //�����
 glColor3f(0.9,0,0);
 line(t[18],t[19],t[22],t[23], 0, 0.2);
 line(t[20],t[21],t[24],t[25], 0, 0.2);
 glEnd();
 
 //������
 glColor3f(0.9, 0.9, 0.2);
 //putpixel(t[26],t[27],1,0);
 
 glPushMatrix();
 glTranslatef(transformX(t[26]), transformY(t[27]), 0.21);
 gluDisk(quad, transformX(razmer * 0.260), transformX(razmer * 0.262), 8, 1);
 glPopMatrix();
}
/*
void obraz0()
{
double a,b,c,d;
  
  //circle(koordinati.x, koordinati.y, 1, radius, 0);   

  a=koordinati.x + (maxx/200.0)*cos(koordinati.posoka);
  b=koordinati.x + (maxy/40.0)*cos(koordinati.posoka);
  c=koordinati.y + (maxx/200.0)*sin(koordinati.posoka);
  d=koordinati.y + (maxy/40.0)*sin(koordinati.posoka);
  
  glColor3f( 0.83, 0.83, 0.66 );
  glEnable(GL_POINT_SMOOTH);
  glEnable(GL_LINE_SMOOTH);
  glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);
  glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
  glPushMatrix();
  glTranslatef(transformX(koordinati.x), transformY(koordinati.y), 0);
  gluDisk(quad, 0, maxy / 30000.0, maxy / 50, 1);
  glPopMatrix();
  
  line(a,c,b,d,0,0);

  nos.x = b;
  nos.y = d;
  nos.posoka = koordinati.posoka;
}
*/
void polet(double Cdelay)
{
	if (Cdelay - vreme.zavoi >= timeout.zavoi)
	{
		if (kop.a)
		{
			// �������� �� �������� * ����������� �� ����������
			koordinati.posoka -= 0.06 * (Cdelay - vreme.zavoi) / timeout.zavoi;
			if (koordinati.posoka < 0) koordinati.posoka=M_2PI;
		}

		if (kop.d) 
		{
			// �������� �� �������� * ����������� �� ����������
			koordinati.posoka += 0.06 * (Cdelay - vreme.zavoi) / timeout.zavoi;
			if (koordinati.posoka > M_2PI) koordinati.posoka=0;
		}

		vreme.zavoi = Cdelay;
	}

	if (Cdelay - vreme.napred >= timeout.napred)
	{  
		// ������ * ����������� �� ����������
		if (kop.w)
		{
			koordinati.y += (maxy/300.0) * sin(-koordinati.posoka) * (Cdelay - vreme.napred) / timeout.napred;
			koordinati.x += (maxy/300.0) * cos(-koordinati.posoka) * (Cdelay - vreme.napred) / timeout.napred;

			if (koordinati.x>=maxx) koordinati.x=2;
			if (koordinati.y>=maxy) koordinati.y=2;
			if (koordinati.x<=1) koordinati.x=maxx;
			if (koordinati.y<=1) koordinati.y=maxy;

			zvuk.timeout.gorivo = -2;
		}
		
		vreme.napred = Cdelay;
	}
}

void smrt(coords *koord, ship::coords *vzrivove, int max, int max2)
{
  const unsigned short lim=max2+max;
  unsigned j,i;
  double radius2=radius*radius;
  
     for (j=max; j<lim; j++)
         {
			 if (koord[j].posoka==200) continue;
			 
             double d1=razstoqnie(koord[j].x, koord[j].y, koordinati.x, koordinati.y);
			         
			 if (d1 <= radius2)
                {
				 krv-=izkarvane_na_krv[5];
                 
                 if (krv>0)
                    {
                    for (i=10; i<20; i++)
                       if (!vzrivove[i].posoka)
		               {
		                vzrivove[i].posoka = koord[j].posoka - M_2PI;
                        vzrivove[i].x = koord[j].x;
                        vzrivove[i].y = koord[j].y;
                        break;
		               }
                    }
				 else
				    {
				     jivoti--;
				     krv=100;
	                 
	                 if (izkarvane_na_krv[4] / 1.4641 < 0.13) izkarvane_normalno();
	                 else for(i=0; i<5; i++) izkarvane_na_krv[i] /= 1.4641;
	                 
				     vzrivove[0].x=koordinati.x;
				     vzrivove[0].y=koordinati.y;
				     vzrivove[0].posoka=koord[j].posoka;
				      if (jivoti)
				         {
				           koordinati.x=DisplayParameters.x / 2;
				           koordinati.y=DisplayParameters.y / 2;
				           koordinati.posoka = M_PI2;
		                 }
		              else radius=0;
		            }
    		     koord[j].posoka=200;
                }
          }
 }    

};

class bullet
{
private:
  double a,b,c,d;
  unsigned char max,max_zli;
  unsigned i;
  struct _vreme { double napred, nova; };

public:
  double radius;
  unsigned int maxx, maxy;
  ship::coords *koordinati;
  _vreme vreme, timeout;

bullet(){}
bullet(const unsigned char maxBullets, const unsigned char maxEnemies)
{
  max=maxBullets;
  max_zli=maxEnemies+max;
  koordinati = new ship::coords [max_zli];
  
  for (i=0;i<maxBullets;i++) koordinati[i].posoka=200;
}

~bullet() {}

void obraz()
{
 const double __radius1=maxx/150.0,
              __radius2=maxy/150.0;

 glLineWidth(2);
 glEnable(GL_LINE_SMOOTH);
 glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
 glEnable(GL_BLEND);
 glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
 glBegin(GL_LINES);
 
 for (i=0; i<max_zli; i++)
     {
       if (koordinati[i].posoka==200) continue;
       
       if (i>=max) glColor3f(1.0, 0.3, 0.0);
       else        glColor3f( 0.1, 1.26 - izkarvane_na_krv[4]*2.0, izkarvane_na_krv[4]*6.0 - 0.78 );
       
       a=koordinati[i].x - __radius1*cos(koordinati[i].posoka);
       c=koordinati[i].x + __radius2*cos(koordinati[i].posoka);
       b=koordinati[i].y - __radius1*sin(koordinati[i].posoka);
       d=koordinati[i].y + __radius2*sin(koordinati[i].posoka);
       line(a,b,c,d,-0.3,1);
      // circle(koordinati[i].x, koordinati[i].y, 0.5, radius, 0);
     }
 
 glEnd();
}

void polet(double Cdelay)
{
	if (Cdelay - vreme.napred < timeout.napred) return;
 
	const double stpka = maxx/100.0 * (Cdelay - vreme.napred) / timeout.napred;
 
	for (i=0; i<max_zli; i++)
	{
		koordinati[i].x += stpka * cos( koordinati[i].posoka );
		koordinati[i].y += stpka * sin( koordinati[i].posoka );

		if ((koordinati[i].x >= maxx || koordinati[i].x < 0) || (koordinati[i].y >= maxy || koordinati[i].y < 0))
			koordinati[i].posoka = 200;
	}

	vreme.napred = Cdelay;
}

void nova(ship::coords *poziciq, bool zlo)
{
bool da = 0;
unsigned char lim, start;

 if (zlo) {lim=max_zli; start=max;}
 else     {lim=max; start=0;}

 for (i=start; i<lim; i++)
     if (koordinati[i].posoka==200)
         {
         koordinati[i].posoka=poziciq->posoka;
         koordinati[i].x=poziciq->x;
         koordinati[i].y=poziciq->y;
         //zvuk.timeout.proba=-2;
         da = 1;
         break;
         }
 if (zlo) poziciq->posoka=200;
 else 
     if (da) zvuk.timeout.kurszum = -2;
}

void kontrol(double Cdelay, ship::coords korab_koordinati)
{      
  if ( (Cdelay - vreme.nova < timeout.nova) || (!kop.tab) ) return;
  vreme.nova = Cdelay;
    
  nova(&korab_koordinati, 0);
}

};

class _zvezdi
{
private:
unsigned i, max;

ship::coords *zvezdi;

public:

_zvezdi(){}

void init(unsigned maxx, unsigned maxy)
{
  max = maxx / 7;
  randomize();
  zvezdi = new ship::coords[max];
   
  for (i=0; i<max; i++)
      {
        zvezdi[i].posoka = random(100) / 100.0;
        zvezdi[i].x = random(maxx);
        zvezdi[i].y = random(maxy);
      }
}

~_zvezdi(){}

void obraz()
{
 float cvqt;
 
 glDisable(GL_POINT_SMOOTH);
 glDisable(GL_BLEND);
 glPointSize(2);
 glBegin(GL_POINTS);
 
 for (i=0; i<max; i++)
     {
      cvqt = zvezdi[i].posoka - (random(100) * 0.002);
      glColor3f(cvqt, cvqt, cvqt);
      putpixel(zvezdi[i].x, zvezdi[i].y, -0.9, -1);
     }
 
 glEnd();
}

} zvezdi;
