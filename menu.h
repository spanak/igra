﻿#pragma once

#include <time.h>
#include <stdlib.h>

#include "cfg.h"
#include "graphics.h"

struct __fonts {unsigned hud, menu;} font;

unsigned char ezik, izbrano=1, izbrani_max=4, izbrani_min=0, _menu=0, nivo=0,
              star_kop;
char ime[9], ime_broene=0, ludi_imena[3][9]={"Kondio", "Shagrath", "Burzum"};

int razdelitelna=1, stara_razdelitelna=-1,
    OldRefresh = 0,
        OldBPP = 0;
bool     OldFS = 0,
          igra = 0,
         clear_w=0, clear_a=0, clear_d=0, clear_tab=0, testova_igra=0;

float ludi[3], toczki;
double maxx=0, maxy=0, selection_t[6];

const char *zaglavie="igra 713a",
           *crossfire_volunteer[2]={"Доброволец за престрелка", "Crossfire volunteer"},
           *msg_nova[2]={"Нова игра", "Nеw Game"},
           *msg_stiga_igrane[2]={"Край на играта", "End Game"},
           *msg_nastroiki[2]={"Настройки", "Options"},
           *msg_izhod[2]={"Изход", "Exit"},
           *msg_prodlji[2]={"Продължи", "Continue"},
           *msg_res[2]={"Резолюция:", "Resolution:"},
           *msg_nazad[2]={"Назад", "Back"},
           *msg_kop[2]={"Копчета", "Controls"},
           *msg_w[2]={"Напред:", "Forward:"},
           *msg_a[2]={"Наляво:", "Left:"},
           *msg_d[2]={"Надясно:", "Right:"},
           *msg_tab[2]={"Стрелба:", "Shoot:"},
           *msg_cvqt[2]={"Цветове:", "Colour Depth:"},
           *msg_refresh[2]={"Честота:", "Refresh Rate:"},
           *msg_ezik[2]={"Език:","Language:"},
           *msg_imeNaEzik[2]={"Български", "English"},
           *msg_CqlEkran[2]={"Цял екран:", "Full Screen:"},
           *msg_da[2]={"Да", "Yes"},
           *msg_ne[2]={"Не", "No"},
           *msg_default_keys[2]={"Назад: ESC, Изход: F12", "Back: ESC, Exit: F12"},
           *msg_standartni_kopczeta[2]={"Стандартни (F5)", "Deafult (F5)"},
           *msg_krai_na_igrata[2]={"Край на играта","Game Over"},
           *msg_peczelisz[2]={"Печелиш!", "You win!"},
           *msg_test[2]={"Тест на системата", "System Test"},
           *msg_rekord[2]={"Постави нов рекорд !", "You set a high score !"},
           *msg_ime[2]={"Въведи името си:", "Enter your name:"},
           *msg_ludi[2]={"Рекорди", "High Scores"},
           *msg_zvuk[2]={"Звук", "Sound"},
           *msg_zvuk_sila[2]={"Сила: ", "Volume: "},
           *msg_zvuk_hint[2]={"Може да ползвате също и F7 и F8 по време на игра.", "You can also use F7 and F8 while playing."};

class control
{
public:
  short W, A, D, TAB, ESC;
  bool w, a, d, tab, F2, F11, F9, F7, F8, esc, up, down, enter, spri_proverka, shift, left, right;

control()
{
W=0;A=W;D=W;TAB=W;ESC=W;
spri_proverka=(W && 1);
}

~control(){}

void DefaultKeys()
{
W=104;
A=102;
D=100;
TAB=9;
ESC=VK_ESCAPE;
}

void SetKey(short *key, bool wait)
{
int i;

  for (i=8; i<256; i++)
      {
	    switch (i)
		{
		case VK_F1: i=VK_F5; break;
		case VK_F6: i=0x90; break;
		case 0x0a: i+=2; break;
		case 0x0e: i+=5; break;
		case 0x15: i+=11; break;
		case 0x3a: i+=6; break;
		case 0x5e: i+=1; break;
		case 0x88: i+=7; break;
		case 0x97: i+=8; break;
		case 0xb8: i+=2; break;
		case 0xc1: i+=0x1a; break;
		case 0xe0: i+=1; break;
		case 0xe8: i+=1; break;
		case 0xfc: i+=1; break;
	    case 0xff: if (wait) {i=0; break;}
	               else      {*key=0; return;}
		}
     
	  if (GetAsyncKeyState(i)) 
         {
           if (i==VK_F5) {DefaultKeys();return;}
           *key=i;return;
         }
      }

}

void prevod(int kopcze, char *retval)
{
 switch (kopcze)
        {
         case 0:     strcpy(retval, " ");break;
         case 1:     strcpy(retval, "Mouse Left");break;
         case 2:     strcpy(retval, "Mouse Right");break;
         case 3:     strcpy(retval, "Ctrl + Break");break;
         case 4:     strcpy(retval, "Mouse X1");break;
         case 5:     strcpy(retval, "Mouse X2");break;
         case 8:     strcpy(retval, "Backspace");break;
         case 9:     strcpy(retval, "Tab");break;
         case 0xc:   strcpy(retval, "Clear");break;
         case 0xd:   strcpy(retval, "Enter");break;
         case 0x13:  strcpy(retval, "Pause");break;
         case 0x14:  strcpy(retval, "Caps Lock");break;
         case 0x20:  strcpy(retval, "Space");break;
         case 0x21:  strcpy(retval, "Page Up");break;
         case 0x22:  strcpy(retval, "Page Down");break;
         case 0x23:  strcpy(retval, "End");break;
         case 0x24:  strcpy(retval, "Home");break;
         case 0x25:  strcpy(retval, "Left");break;
         case 0x26:  strcpy(retval, "Up");break;
         case 0x27:  strcpy(retval, "Right");break;
         case 0x28:  strcpy(retval, "Down");break;
         case 0x29:  strcpy(retval, "Select");break;
         case 0x2a:  strcpy(retval, "Print");break;
         case 0x2b:  strcpy(retval, "Execute");break;
         case 0x2c:  strcpy(retval, "Print Screen");break;
         case 0x2d:  strcpy(retval, "Insert");break;
         case 0x2e:  strcpy(retval, "Delete");break;
         case 0x2f:  strcpy(retval, "Help");break;
         case 0x30:  strcpy(retval, "0");break;
         case 0x31:  strcpy(retval, "1");break;
         case 0x32:  strcpy(retval, "2");break;
         case 0x33:  strcpy(retval, "3");break;
         case 0x34:  strcpy(retval, "4");break;
         case 0x35:  strcpy(retval, "5");break;
         case 0x36:  strcpy(retval, "6");break;
         case 0x37:  strcpy(retval, "7");break;
         case 0x38:  strcpy(retval, "8");break;
         case 0x39:  strcpy(retval, "9");break;
         case 0x41:  strcpy(retval, "A");break;
         case 0x42:  strcpy(retval, "B");break;
         case 0x43:  strcpy(retval, "C");break;
         case 0x44:  strcpy(retval, "D");break;
         case 0x45:  strcpy(retval, "E");break;
         case 0x46:  strcpy(retval, "F");break;
         case 0x47:  strcpy(retval, "G");break;
         case 0x48:  strcpy(retval, "H");break;
         case 0x49:  strcpy(retval, "I");break;
         case 0x4a:  strcpy(retval, "J");break;
         case 0x4b:  strcpy(retval, "K");break;
         case 0x4c:  strcpy(retval, "L");break;
         case 0x4d:  strcpy(retval, "M");break;
         case 0x4e:  strcpy(retval, "N");break;
         case 0x4f:  strcpy(retval, "O");break;
         case 0x50:  strcpy(retval, "P");break;
         case 0x51:  strcpy(retval, "Q");break;
         case 0x52:  strcpy(retval, "R");break;
         case 0x53:  strcpy(retval, "S");break;
         case 0x54:  strcpy(retval, "T");break;
         case 0x55:  strcpy(retval, "U");break;
         case 0x56:  strcpy(retval, "V");break;
         case 0x57:  strcpy(retval, "W");break;
         case 0x58:  strcpy(retval, "X");break;
         case 0x59:  strcpy(retval, "U");break;
         case 0x5a:  strcpy(retval, "Z");break;
         case 0x5b:  strcpy(retval, "Left Windows");break;
         case 0x5c:  strcpy(retval, "Right Windows");break;
         case 0x5d:  strcpy(retval, "Context Menu");break;
         case 0x5f:  strcpy(retval, "Sleep");break;
         case 0x60:  strcpy(retval, "Numeric Pad 0");break;
         case 0x61:  strcpy(retval, "Numeric Pad 1");break;
         case 0x62:  strcpy(retval, "Numeric Pad 2");break;
         case 0x63:  strcpy(retval, "Numeric Pad 3");break;
         case 0x64:  strcpy(retval, "Numeric Pad 4");break;
         case 0x65:  strcpy(retval, "Numeric Pad 5");break;
         case 0x66:  strcpy(retval, "Numeric Pad 6");break;
         case 0x67:  strcpy(retval, "Numeric Pad 7");break;
         case 0x68:  strcpy(retval, "Numeric Pad 8");break;
         case 0x69:  strcpy(retval, "Numeric Pad 9");break;
         case 0x6a:  strcpy(retval, "*");break;
         case 0x6b:  strcpy(retval, "+");break;
         case 0x6c:  strcpy(retval, "Separator");break;
         case 0x6d:  strcpy(retval, "-");break;
         case 0x6e:  strcpy(retval, ".");break;
         case 0x6f:  strcpy(retval, "/");break;
         case 0x90:  strcpy(retval, "Num Lock");break;
         case 0x91:  strcpy(retval, "Scroll Lock");break;
         case 0xa0:  strcpy(retval, "Left Shift");break;
         case 0xa1:  strcpy(retval, "Right Shift");break;
         case 0xa2:  strcpy(retval, "Left Control");break;
         case 0xa3:  strcpy(retval, "Right Control");break;
         case 0xa4:  strcpy(retval, "Left Alt");break;
         case 0xa5:  strcpy(retval, "Right Alt");break;
         case 0xba:  strcpy(retval, ";");break;
         case 0xbb:  strcpy(retval, "+");break;
         case 0xbc:  strcpy(retval, ",");break;
         case 0xbd:  strcpy(retval, "-");break;
         case 0xbe:  strcpy(retval, ".");break;
         case 0xbf:  strcpy(retval, "/");break;
         case 0xc0:  strcpy(retval, "~");break;
         case 0xdb:  strcpy(retval, "[");break;
         case 0xdc:  strcpy(retval, "\\");break;
         case 0xdd:  strcpy(retval, "]");break;
         case 0xde:  strcpy(retval, "'");break;
         
         default:    itoa(kopcze, retval, 10);
        }
}

void proverka()
{
  if ( FindWindow(NULL, zaglavie) != GetFocus() ) 
     {
      up=0;down=up;enter=up;shift=up;F2=up;F9=up;F11=up;esc=up;
      F7=up;F8=up;left=up;right=up;
      goto clear_wasd;
     }
  
  if (GetAsyncKeyState(VK_F12)) {zvuk.stop();exit(1);}
  
  up=(GetAsyncKeyState(VK_UP) && 1);
  down=(GetAsyncKeyState(VK_DOWN) && 1);
  left=(GetAsyncKeyState(VK_LEFT) && 1);
  right=(GetAsyncKeyState(VK_RIGHT) && 1);
  enter=(GetAsyncKeyState(VK_RETURN) && 1);
  shift=(GetAsyncKeyState(VK_SHIFT) && 1);
  
  F2=(GetAsyncKeyState(VK_F2) && 1);
  F7=(GetAsyncKeyState(VK_F7) && 1);
  F8=(GetAsyncKeyState(VK_F8) && 1);
  F9=(GetAsyncKeyState(VK_F9) && 1);
  F11=(GetAsyncKeyState(VK_F11) && 1);
  esc=(GetAsyncKeyState(ESC) && 1);
  
  if (spri_proverka) 
     {
clear_wasd:
       w=0;a=w;d=w;tab=w;
       return;
     }
  
  w=(GetAsyncKeyState(W) && 1);
  a=(GetAsyncKeyState(A) && 1);
  d=(GetAsyncKeyState(D) && 1);
  tab=(GetAsyncKeyState(TAB) && 1);
  
  if (a && d) {a=0;d=0;}
}

} kop;

void kosmos();
void __mainLoop();
void lud_kontrol();
void lud_ekran();

void DefaultSettings(bool restart, bool golqm_restart);

void triygylnik()
{
selection_t[0]=DisplayParameters.x*0.275;    //220
selection_t[1]=DisplayParameters.x*0.24375; //195
selection_t[2]=selection_t[0];
selection_t[3]=DisplayParameters.x*0.26875; //215
selection_t[4]=DisplayParameters.x*0.2875;  //230
selection_t[5]=DisplayParameters.x*0.25625; //205
}

void CFGSettings()
{
 float tmp3;
 unsigned tmp2=10;
 char tmp[256];
 
 strcpy (tmp, cfg.value("razdelitelna="));
 if (strcmp(tmp, "-1")) tmp2 = atoi(tmp);
 if (tmp2 <= 8) razdelitelna = tmp2;
 
  switch (razdelitelna)
                      {
                        case 0: DisplayParameters.x = 640; break;
                        case 1: DisplayParameters.x = 800; break;
                        case 2: DisplayParameters.x = 960; break;
                        case 3: DisplayParameters.x = 1024; break;
                        case 4: DisplayParameters.x = 1152; break;
                        case 5: DisplayParameters.x = 1280; break;
                        case 6: DisplayParameters.x = 1440; break;
                        case 7: DisplayParameters.x = 1600; break;
                        case 8: DisplayParameters.x = 2048; break;
                      }
                      
 DisplayParameters.y = 0.75 * DisplayParameters.x;
 
 ///////////////////////////////////////
 tmp2=0;
 strcpy (tmp, cfg.value("cvetove="));
 if (strcmp(tmp, "-1")) tmp2 = atoi(tmp);
 if (tmp2==8 || tmp2==16 || tmp2==32) DisplayParameters.BitsPerPixel=tmp2;
 
 ///////////////////////////////////////
 strcpy (tmp, cfg.value("czestota="));
 if (strcmp(tmp, "-1")) tmp2 = atoi(tmp);
 switch (tmp2)
        {
        case 150:
        case 120:
        case 100:
        case 90:
        case 85:
        case 75:
        case 70:
        case 60: DisplayParameters.RefreshRate=tmp2;
        }
 
 ///////////////////////////////////////
 tmp2=0;
 strcpy (tmp, cfg.value("cqlEkran="));
 if (strcmp(tmp, "-1")) tmp2 = atoi(tmp);
 if (tmp2 == 1 || !tmp2) DisplayParameters.FullScreen = (tmp2 && 1);

 ///////////////////////////////////////
 tmp2=0;
 strcpy (tmp, cfg.value("ezik="));
 if (strcmp(tmp, "-1")) tmp2 = atoi(tmp);
 if (tmp2 == 1 || !tmp2) ezik = tmp2;
 
 ///////////////////////////////////////
 tmp2=0;
 strcpy (tmp, cfg.value("zvuk="));
 if (strcmp(tmp, "-1")) 
    {
     tmp2 = atoi(tmp);
     if ((tmp2 < 128) && (zvuk.zvuk != -1)) zvuk.zvuk = (tmp2 & 0xFF);
    }
 
 ///////////////////////////////////////
 tmp2=0xfe;
 strcpy (tmp, cfg.value("W="));
 if (strcmp(tmp, "-1")) tmp2 = atoi(tmp);
 if (tmp2 < 0xdf) kop.W = tmp2;

 tmp2=0xfe;
 strcpy (tmp, cfg.value("A="));
 if (strcmp(tmp, "-1")) tmp2 = atoi(tmp);
 if (tmp2 < 0xdf) kop.A = tmp2;
 
 tmp2=0xfe;
 strcpy (tmp, cfg.value("D="));
 if (strcmp(tmp, "-1")) tmp2 = atoi(tmp);
 if (tmp2 < 0xdf) kop.D = tmp2;
 
 tmp2=0xfe;
 strcpy (tmp, cfg.value("TAB="));
 if (strcmp(tmp, "-1")) tmp2 = atoi(tmp);
 if (tmp2 < 0xdf) kop.TAB = tmp2;

 ///////////////////////////////////////
 tmp3=0;
 strcpy(tmp, cfg.value("lut1="));
 if (strcmp(tmp, "-1")) tmp3 = atof(tmp);
 if (tmp3>=0) ludi[0]=tmp3;
 else         ludi[0]=0;

 tmp3=0;
 strcpy(tmp, cfg.value("lut2="));
 if (strcmp(tmp, "-1")) tmp3 = atof(tmp);
 if (tmp3>=0) ludi[1]=tmp3;
 else         ludi[1]=0;
 
 tmp3=0;
 strcpy(tmp, cfg.value("lut3="));
 if (strcmp(tmp, "-1")) tmp3 = atof(tmp);
 if (tmp3>=0) ludi[2]=tmp3;
 else         ludi[2]=0;
 
 strcpy(tmp, cfg.value("lud1="));
 if (strcmp(tmp, "-1")) strcpy(ludi_imena[0], tmp);

 strcpy(tmp, cfg.value("lud2="));
 if (strcmp(tmp, "-1")) strcpy(ludi_imena[1], tmp);
 
 strcpy(tmp, cfg.value("lud3="));
 if (strcmp(tmp, "-1")) strcpy(ludi_imena[2], tmp);
 
 DefaultSettings(0,1);
}

void menu()
{
char tmp[256];
  
  //_menu = ;
  
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);  
  glColor3f(0.7, 0.7, 0.7);
  strcpy(tmp, msg_ezik[0]);
  strcat(tmp, " / ");
  strcat(tmp, msg_ezik[1]);
  strcat(tmp, " [ F8 ]");
  OutTextXY(DisplayParameters.y * 0.004, DisplayParameters.y * 0.02, 0, font.hud, zaglavie);
  OutTextXY(DisplayParameters.x * 0.809, DisplayParameters.y * 0.02, 0, font.hud, "Dimo - 2007-2008, 2014");
  OutTextXY(DisplayParameters.x * 0.004, DisplayParameters.y * 0.992, 0, font.hud, tmp);
  {
         float x;
         if (!ezik) x=DisplayParameters.x * 0.25;
         else       x=DisplayParameters.x * 0.325;
         glColor3f(0.6, 1, 0.6);
         OutTextXY(x, DisplayParameters.x * 0.175, 0, font.menu, crossfire_volunteer[ezik]);
  }
  glColor3f(1, 1, 1);
  
  switch (_menu)
  {
  case 0:
      if (igra)
         {
         OutTextXY(DisplayParameters.y * 0.4, DisplayParameters.x * 0.26875, 0, font.menu,msg_prodlji[ezik]);
         OutTextXY(DisplayParameters.y * 0.4, DisplayParameters.x * 0.3125, 0, font.menu,msg_stiga_igrane[ezik]);
         }
      else
         {
         OutTextXY(DisplayParameters.y * 0.4, DisplayParameters.x * 0.26875, 0, font.menu,msg_test[ezik]);
         OutTextXY(DisplayParameters.y * 0.4, DisplayParameters.x * 0.3125, 0, font.menu,msg_nova[ezik]);
         }
      OutTextXY(DisplayParameters.y * 0.4, DisplayParameters.y * 0.475, 0, font.menu,msg_nastroiki[ezik]);
      OutTextXY(DisplayParameters.y * 0.4, DisplayParameters.x * 0.4, 0, font.menu,msg_ludi[ezik]);
      OutTextXY(DisplayParameters.y * 0.4, DisplayParameters.x * 0.44375, 0, font.menu,msg_izhod[ezik]);

  break;
  case 1:
      OutTextXY(DisplayParameters.y * 0.4, DisplayParameters.x * 0.26875, 0, font.menu,msg_res[ezik]);      
      switch (razdelitelna)
             {
               case 0: strcpy(tmp,"640x480"); break;
               case 1: strcpy(tmp,"800x600"); break;
               case 2: strcpy(tmp,"960x720"); break;
               case 3: strcpy(tmp,"1024x768"); break;
               case 4: strcpy(tmp,"1152x864"); break;
               case 5: strcpy(tmp,"1280x960"); break;
               case 6: strcpy(tmp,"1440x1080"); break;
               case 7: strcpy(tmp,"1600x1200"); break;
               case 8: strcpy(tmp,"2048x1536"); break;
             }

      OutTextXY(DisplayParameters.x * 0.56, DisplayParameters.x * 0.26875, 0, font.menu, tmp);
      
      OutTextXY(DisplayParameters.y * 0.4, DisplayParameters.x * 0.3125, 0, font.menu, msg_cvqt[ezik]);
      OutTextXY(DisplayParameters.x * 0.56, DisplayParameters.x * 0.3125, 0, font.menu, "%d-bit", DisplayParameters.BitsPerPixel);
      
      OutTextXY(DisplayParameters.y * 0.4, DisplayParameters.y * 0.475, 0, font.menu,msg_refresh[ezik]);
      OutTextXY(DisplayParameters.x * 0.56, DisplayParameters.y * 0.475, 0, font.menu, "%d Hz", DisplayParameters.RefreshRate);      

      if (DisplayParameters.FullScreen) strcpy(tmp,msg_da[ezik]);
      else                              strcpy(tmp,msg_ne[ezik]);
      OutTextXY(DisplayParameters.y * 0.4, DisplayParameters.x * 0.4, 0, font.menu,msg_CqlEkran[ezik]);
      OutTextXY(DisplayParameters.x * 0.56, DisplayParameters.x * 0.4, 0, font.menu,tmp);
      
      if (zvuk.zvuk < 0) glColor3f(0.7, 0.7, 0.7);
      OutTextXY(DisplayParameters.y * 0.4, DisplayParameters.x * 0.44375, 0, font.menu,msg_zvuk[ezik]);   
      if (zvuk.zvuk < 0) glColor3f(1, 1, 1);
      
      OutTextXY(DisplayParameters.y * 0.4, DisplayParameters.x * 0.4875, 0, font.menu,msg_kop[ezik]);   
      
      OutTextXY(DisplayParameters.y * 0.4, DisplayParameters.x * 0.53125, 0, font.menu,msg_ezik[ezik]);      
      OutTextXY(DisplayParameters.x * 0.56, DisplayParameters.x * 0.53125, 0, font.menu, msg_imeNaEzik[ezik]);      

      OutTextXY(DisplayParameters.y * 0.4, DisplayParameters.x * 0.575, 0, font.menu,msg_nazad[ezik]);  
        
  break;
  case 2:
     // strcpy(tmp,kop.prevod(kop.W));
      kop.prevod(kop.W, tmp);
      OutTextXY(DisplayParameters.y * 0.4, DisplayParameters.x * 0.26875, 0, font.menu,msg_w[ezik]);
      OutTextXY(DisplayParameters.x * 0.56, DisplayParameters.x * 0.26875, 0, font.menu, tmp);
      
      //strcpy(tmp,kop.prevod(kop.A));
      kop.prevod(kop.A, tmp);
      OutTextXY(DisplayParameters.y * 0.4, DisplayParameters.x * 0.3125, 0, font.menu,msg_d[ezik]);
      OutTextXY(DisplayParameters.x * 0.56, DisplayParameters.x * 0.3125, 0, font.menu, tmp);
      
      //strcpy(tmp,kop.prevod(kop.D));
      kop.prevod(kop.D, tmp);
      OutTextXY(DisplayParameters.y * 0.4, DisplayParameters.y * 0.475, 0, font.menu,msg_a[ezik]);
      OutTextXY(DisplayParameters.x * 0.56, DisplayParameters.y * 0.475, 0, font.menu, tmp);

      //strcpy(tmp,kop.prevod(kop.TAB));
      kop.prevod(kop.TAB, tmp);
      OutTextXY(DisplayParameters.y * 0.4, DisplayParameters.x * 0.4, 0, font.menu, msg_tab[ezik]);
      OutTextXY(DisplayParameters.x * 0.56, DisplayParameters.x * 0.4, 0, font.menu, tmp);
      
      OutTextXY(DisplayParameters.y * 0.4, DisplayParameters.x * 0.44375, 0, font.menu, msg_standartni_kopczeta[ezik]);

      glColor3f(0.7, 0.7, 0.7);
      OutTextXY(DisplayParameters.y * 0.4, DisplayParameters.x * 0.4875, 0, font.menu,msg_default_keys[ezik]);   
      glColor3f(1, 1, 1);
      
      OutTextXY(DisplayParameters.y * 0.4, DisplayParameters.x * 0.53125, 0, font.menu,msg_nazad[ezik]);    
  break;
  case 3:
      OutTextXY(DisplayParameters.y * 0.4, DisplayParameters.x * 0.26875, 0, font.menu, ludi_imena[0]);
      OutTextXY(DisplayParameters.x * 0.56, DisplayParameters.x * 0.26875, 0, font.menu, "%1.0lf", ludi[0]);

      OutTextXY(DisplayParameters.y * 0.4, DisplayParameters.x * 0.3125, 0, font.menu, ludi_imena[1]);
      OutTextXY(DisplayParameters.x * 0.56, DisplayParameters.x * 0.3125, 0, font.menu, "%1.0lf", ludi[1]);

      OutTextXY(DisplayParameters.y * 0.4, DisplayParameters.y * 0.475, 0, font.menu,  ludi_imena[2]);
      OutTextXY(DisplayParameters.x * 0.56, DisplayParameters.y * 0.475, 0, font.menu, "%1.0lf", ludi[2]);

      OutTextXY(DisplayParameters.y * 0.4, DisplayParameters.x * 0.44375, 0, font.menu,msg_nazad[ezik]);
  break;
  case 4:
      OutTextXY(DisplayParameters.y * 0.4, DisplayParameters.x * 0.26875, 0, font.menu, msg_zvuk_sila[ezik]);
      OutTextXY(DisplayParameters.x * 0.56, DisplayParameters.x * 0.26875, 0, font.menu, "%d%%", 100 * zvuk.zvuk / 127);
      
      OutTextXY(DisplayParameters.y * 0.4, DisplayParameters.x * 0.3125, 0, font.menu, msg_nazad[ezik]);
      
      glColor3f(0.7, 0.7, 0.7);
      OutTextXY(DisplayParameters.y * 0.4, DisplayParameters.y * 0.475, 0, font.hud, msg_zvuk_hint[ezik]);
      glColor3f(1, 1, 1);
  }
  
  glBegin(GL_TRIANGLES);
  glVertex3f(transformX(selection_t[0]),transformY(selection_t[1]+izbrano*DisplayParameters.x*0.04375),0);
  glVertex3f(transformX(selection_t[2]),transformY(selection_t[3]+izbrano*DisplayParameters.x*0.04375),0);
  glVertex3f(transformX(selection_t[4]),transformY(selection_t[5]+izbrano*DisplayParameters.x*0.04375),0);
  glEnd();
  
  glutPostRedisplay();
  glutSwapBuffers();
}

void menu_kontrol()
{
int i;
double Cdelay;

 Cdelay=clock();
 
 kop.proverka();
 if (clear_w) goto set_w;
 if (clear_a) goto set_a;
 if (clear_d) goto set_d;
 if (clear_tab) goto set_tab;
 
 if (kop.F8 && star_kop!='1') {star_kop = '1'; if (++ezik > 1) ezik = 0;}
 if (!kop.F8 && star_kop=='1') star_kop = 0;
 
 if (!kop.left && star_kop=='a') star_kop=0;
 if (!kop.right && star_kop=='d') star_kop=0;                                                     
 
 if (!kop.down && star_kop=='s') star_kop=0;
 if (!kop.up && star_kop=='w') star_kop=0;
 if (!kop.enter && star_kop=='e') star_kop=0;
 
 if (kop.esc) izbrano=izbrani_max;
 if (kop.down && izbrano < izbrani_max && star_kop!='s') {izbrano++;star_kop='s';}
 if (kop.up && izbrano > izbrani_min && star_kop!='w') {izbrano--;star_kop='w';} 
 
 if (kop.left && (star_kop!='a')) 
    {
     if (_menu==4)
        {
         if (!izbrano)
           if (zvuk.zvuk >= 10) zvuk.zvuk -= 10;
           else zvuk.zvuk = 0;
        }
     else if (_menu==1)
        
        switch (izbrano)
        {
         case 0:
                if (stara_razdelitelna<0) stara_razdelitelna = razdelitelna; 
                if (razdelitelna > 0) razdelitelna--;
         break;
         
         case 1: if (!OldBPP) OldBPP = DisplayParameters.BitsPerPixel;
                 if (DisplayParameters.BitsPerPixel > 8) DisplayParameters.BitsPerPixel /= 2;
                 break;
        
         case 2:
             if (!OldRefresh) OldRefresh = DisplayParameters.RefreshRate;
                   switch (DisplayParameters.RefreshRate)
                          {
                           case 150: DisplayParameters.RefreshRate = 120; break;
                           case 120: DisplayParameters.RefreshRate = 100; break;
                           case 100: DisplayParameters.RefreshRate = 90; break;
                           case 90: DisplayParameters.RefreshRate = 85; break;
                           case 85: DisplayParameters.RefreshRate = 75; break;
                           case 75: DisplayParameters.RefreshRate = 70; break;
                           case 70: DisplayParameters.RefreshRate = 60; break;
                          }
               case 3: if (!OldFS) OldFS = 1;
                   DisplayParameters.FullScreen = false;
                   break;
               break;
         
        }
     star_kop='a';
    }
                                         
 if (kop.right && star_kop!='d') 
    {
     if (_menu==4)
         {
          if (!izbrano)
            if (zvuk.zvuk <= 117) zvuk.zvuk += 10;
            else zvuk.zvuk = 127;
         }
     else if (_menu==1)
          
          switch (izbrano)
          {
           case 0:
              if (stara_razdelitelna<0) stara_razdelitelna = razdelitelna; 
              if (razdelitelna < 8) razdelitelna++;
              break;
              
           case 1: if (!OldBPP) OldBPP = DisplayParameters.BitsPerPixel;
                   if (DisplayParameters.BitsPerPixel < 32) DisplayParameters.BitsPerPixel *= 2;
                   break;
          
           case 2:
              if (!OldRefresh) OldRefresh = DisplayParameters.RefreshRate;
                   switch (DisplayParameters.RefreshRate)
                          {
                           case 60: DisplayParameters.RefreshRate = 70; break;
                           case 70: DisplayParameters.RefreshRate = 75; break;
                           case 75: DisplayParameters.RefreshRate = 85; break;
                           case 85: DisplayParameters.RefreshRate = 90; break;
                           case 90: DisplayParameters.RefreshRate = 100; break;
                           case 100: DisplayParameters.RefreshRate = 120; break;
                           case 120: DisplayParameters.RefreshRate = 150; break;
                          }
               break;
               
               case 3: if (!OldFS) OldFS = 1;
                   DisplayParameters.FullScreen = true;
                   break;
         }
     star_kop='d';
    }
 

 if (kop.enter && star_kop!='e')
    {
      if (!_menu)
         {
           switch (izbrano)
           {
            case 0:
               if (!igra) {testova_igra=1;DefaultSettings(1,0);}
               glutIdleFunc(__mainLoop);
               glutDisplayFunc(kosmos);
               break;
           case 1:
               if (!igra) 
                  {
                    igra=1;nivo=1;testova_igra=0;
                    DefaultSettings(1,0);
                    glutIdleFunc(__mainLoop);
                    glutDisplayFunc(kosmos);
                  }
               else 
                  {
                   igra=0;
                   if (ludi[0] < toczki ||
                       ludi[1] < toczki ||
                       ludi[2] < toczki)
                      { 
                       for (i=0; i<9; i++) ime[i]=0;
                       glutIdleFunc(lud_kontrol);
                       glutDisplayFunc(lud_ekran);
                      }
                  }
               break;
           case 2: _menu=1;izbrani_max=7;izbrano=0;
                   break;
           case 3: _menu=3; izbrani_max=4; izbrani_min=4; izbrano=4;
                   break;
           case 4: cfg.rebuild(razdelitelna, DisplayParameters.BitsPerPixel, DisplayParameters.RefreshRate,
                           DisplayParameters.FullScreen, zvuk.zvuk, ezik, kop.W, kop.A, kop.D, kop.TAB,
                           ludi, ludi_imena);
			       cfg.write();
                   exit(1);
           }
         }
      else if (_menu==1)
         { 
           switch (izbrano)
           {                   
           
           case 4: if (zvuk.zvuk >= 0) {_menu=4;izbrano=0;izbrani_max=1;}
                   break;
           
           case 5: _menu=2;izbrano=0;izbrani_max=6;
                   break;
                   
           case 6: if (++ezik > 1) ezik=0;
                   break;
           
           case 7:
               switch (razdelitelna)
                      {
                        case 0: DisplayParameters.x = 640; break;
                        case 1: DisplayParameters.x = 800; break;
                        case 2: DisplayParameters.x = 960; break;
                        case 3: DisplayParameters.x = 1024; break;
                        case 4: DisplayParameters.x = 1152; break;
                        case 5: DisplayParameters.x = 1280; break;
                        case 6: DisplayParameters.x = 1440; break;
                        case 7: DisplayParameters.x = 1600; break;
                        case 8: DisplayParameters.x = 2048; break;
                      }
                      
                      DisplayParameters.y = 0.75 * DisplayParameters.x;
 
               if (RestartGraph(zaglavie, menu_kontrol, menu)) 
                  {
                    razdelitelna = stara_razdelitelna;
                    DisplayParameters.x = maxx;
                    DisplayParameters.y = maxy;
                    if (OldBPP) DisplayParameters.BitsPerPixel = OldBPP;
                    if (OldFS) DisplayParameters.FullScreen = !DisplayParameters.FullScreen;
                    if (OldRefresh) DisplayParameters.RefreshRate = OldRefresh;                    
                    RestartGraph(zaglavie, menu_kontrol, menu);
                  }
               
               if (ezik < 2) CharacterSet = RUSSIAN_CHARSET;
               else          CharacterSet = ANSI_CHARSET;
               glDeleteLists(font.hud, 256);
               glDeleteLists(font.menu, 256);
               font.hud  = BuildFont(zaglavie, "Arial", DisplayParameters.x / 60, FW_BOLD, 0, 0, 0);
               font.menu = BuildFont(zaglavie, "Arial", DisplayParameters.y / 20, FW_BOLD, 0, 0, 0);
                  
               DefaultSettings(0,1);
               _menu=0;
               OldRefresh = _menu;
               OldBPP = _menu;
               OldFS = (_menu && 1);
               stara_razdelitelna = _menu-1;
               izbrani_max=4;
               izbrano=2;
               
               cfg.rebuild(razdelitelna, DisplayParameters.BitsPerPixel, DisplayParameters.RefreshRate,
                           DisplayParameters.FullScreen, zvuk.zvuk, ezik, kop.W, kop.A, kop.D, kop.TAB,
                           ludi, ludi_imena);
               cfg.write();
               
               break;
           }
         }
      else if (_menu == 2)
           {
             while(GetAsyncKeyState(VK_RETURN));
             switch (izbrano)
                    {
                     case 0: kop.W=0; 
                             if (!clear_w) {clear_w=1; return;}
                             
                     set_w:  for (i=0; i<256; i++) if (GetAsyncKeyState(i)) goto set_w;
                             kop.SetKey(&kop.W, 1);
                             while(GetAsyncKeyState(kop.W));
                             clear_w=0;
                             break;
                     
                     case 1: kop.A=0;
                             if (!clear_a) {clear_a=1; return;}

                     set_a:  for (i=0; i<256; i++) if (GetAsyncKeyState(i)) goto set_a;
                             kop.SetKey(&kop.A, 1);
                             while(GetAsyncKeyState(kop.A));
                             clear_a=0;
                             break;
                     
                     case 2: kop.D=0;
                             if (!clear_d) {clear_d=1; return;}
                     
                     set_d:  for (i=0; i<256; i++) if (GetAsyncKeyState(i)) goto set_d;
                             kop.SetKey(&kop.D, 1);
                             while(GetAsyncKeyState(kop.D));
                             clear_d=0;
                             break;
                     
                     case 3: kop.TAB=0;
                             if (!clear_tab) {clear_tab=1; return;}
                     
                   set_tab:  for (i=0; i<256; i++) if (GetAsyncKeyState(i)) goto set_tab;
                             kop.SetKey(&kop.TAB, 1);
                             while(GetAsyncKeyState(kop.TAB));
                             clear_tab=0;
                             break;
                     
                     case 4: kop.DefaultKeys();
                             break;
                     
                     case 6: izbrani_max=7;_menu=1;izbrano=5;
                             break;
                    }
           }
       else if (_menu == 3)
           {
            _menu=0; izbrani_min=0; izbrani_max=4; izbrano=3;
           }
       else //menu == 4
           {
            if (izbrano == 1)  {_menu=1; izbrani_max=7; izbrano=4;}
               
           }
     star_kop='e';
     }

  if (Cdelay == clock()) Sleep(1); //за да не използва 100% процесора
}

void lud_ekran()
{
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);  
   glColor3f(1, 1, 1);
   
   OutTextXY(DisplayParameters.y * 0.4, DisplayParameters.x * 0.3125, 0, font.menu,msg_rekord[ezik]);
   OutTextXY(DisplayParameters.y * 0.4, DisplayParameters.y * 0.475, 0, font.menu,msg_ime[ezik]);
   OutTextXY(DisplayParameters.y * 0.4, DisplayParameters.x * 0.4, 0, font.menu,ime);

   glutPostRedisplay();
   glutSwapBuffers();
}

void lud_kontrol()
{
short _kop;
float tmp;
char tmp2[256];
  
  kop.proverka();
  if (kop.enter && ime_broene) 
     {
       ime_broene=0;
       for (_kop=0; _kop<2 ; _kop++)
            if (ludi[_kop] < ludi[_kop+1])
               {
               tmp = ludi[_kop];
               ludi[_kop] = ludi[_kop+1];
               ludi[_kop+1] = tmp;
               strcpy(tmp2, ludi_imena[_kop]);
               strcpy(ludi_imena[_kop], ludi_imena[_kop+1]);
               strcpy(ludi_imena[_kop+1], tmp2);
               _kop=0;
               }
       
       for (_kop=0; _kop<3; _kop++) 
           if (toczki > ludi[_kop])
              {
               if (_kop < 2) {ludi[2]=ludi[1]; strcpy(ludi_imena[2], ludi_imena[1]);}
               if (!_kop) {ludi[1]=ludi[0]; strcpy(ludi_imena[1], ludi_imena[0]);}
               
               ludi[_kop]=toczki;
               strcpy(ludi_imena[_kop], ime);
               break;
              }
              
       cfg.rebuild(razdelitelna, DisplayParameters.BitsPerPixel, DisplayParameters.RefreshRate,
                   DisplayParameters.FullScreen, zvuk.zvuk, ezik, kop.W, kop.A, kop.D, kop.TAB,
                   ludi, ludi_imena);
       cfg.write();
       
       while (kop.enter) kop.proverka();
       glutDisplayFunc( menu );
       glutIdleFunc(menu_kontrol);
       return;
     }
   
   kop.SetKey(&_kop, 0);
   
   if (!_kop || star_kop==_kop) 
      {
       star_kop=_kop;
       return;
      }
   
   if (_kop==8 && ime_broene>0) ime[--ime_broene]=0;
   
   if (ime_broene==8) return;
   
   if ((_kop > 0x40 && _kop < 0x5b) || (_kop >= 0x30 && _kop <= 0x39) || _kop==32) 
       {
        ime[ime_broene++] = _kop;
        if (!kop.shift && _kop > 0x40) ime[ime_broene-1]+=32;
       }
   star_kop=_kop;
}