#define WIN32_LEAN_AND_MEAN
#pragma warning(disable:4267)
#pragma once

#include <windows.h>
#include <math.h>
#include "glut.h"

//#define transformX(a) DisplayParameters.x_coef * a - 0.5
//#define transformY(b) DisplayParameters.y_coef * (DisplayParameters.y - b)

struct __display
{ double x_coef, y_coef;
  unsigned x, y;
  int BitsPerPixel, RefreshRate;
  bool FullScreen;
} DisplayParameters;

const double M_PI  = 3.14159265358979323846,
             M_2PI = 2.0*M_PI,
             M_PI2 = 0.5*M_PI;

// These variables set the dimensions of the rectanglar region we wish to view.
const double Xmin = 0.0, Xmax = 3.0;
const double Ymin = Xmin, Ymax = Xmax;
GLUquadricObj *quad = gluNewQuadric(); //Required for drawing circles

double transformX(double a) { return DisplayParameters.x_coef * a - 0.5; }
double transformY(double b) { return DisplayParameters.y_coef * (DisplayParameters.y - b); }

bool ChangeResolution(__display display)
{
DEVMODE dmode;

memset(&dmode, 0, sizeof(DEVMODE)); // clear all memory
dmode.dmSize=sizeof(DEVMODE);	    // it's required to specify the size of this struct
dmode.dmPelsWidth = display.x;	            // width and height of desired resolution
dmode.dmPelsHeight = display.y;
dmode.dmBitsPerPel = display.BitsPerPixel;	// color depth; 8/16/32 bit
dmode.dmFields = DM_BITSPERPEL|DM_PELSWIDTH|DM_PELSHEIGHT; //We want to change these fields

if (display.RefreshRate)
   {
     dmode.dmDisplayFrequency = display.RefreshRate; //Change the refresh rate if desired
     dmode.dmFields = (dmode.dmFields | DM_DISPLAYFREQUENCY);
   }
// change resolution, if possible
if (ChangeDisplaySettings(&dmode, CDS_FULLSCREEN) != DISP_CHANGE_SUCCESSFUL)
{
	return 1; //if not possible return error
}

return 0; //nothing went wrong
}

int initgraph(const char *WindowTitle)
{
unsigned x = DisplayParameters.x,
         y = DisplayParameters.y;

while (x!=y)
    {
     if (x>y) x-=y;
     else     y-=x;
    }
DisplayParameters.x_coef = DisplayParameters.x / x;
DisplayParameters.y_coef = DisplayParameters.y / x;
DisplayParameters.x_coef /= DisplayParameters.x;
DisplayParameters.y_coef /= DisplayParameters.y;

    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | DisplayParameters.BitsPerPixel );
    
    if (DisplayParameters.FullScreen)
       { 
         if (ChangeResolution(DisplayParameters)) return 1;
       }
	else
	   {
	     glutInitWindowPosition(10,10);
	     glutInitWindowSize( DisplayParameters.x, DisplayParameters.y );
	   }
	glutCreateWindow(WindowTitle);
    
    if (DisplayParameters.FullScreen) {glutFullScreen();glutSetCursor(GLUT_CURSOR_NONE);}
    
    glEnable ( GL_DEPTH_TEST );

	// The following commands should cause points and line to be drawn larger
	//	than a single pixel width.
//	glPointSize(5);
//	glLineWidth(5);


return 0;
}

// Called when the window is resized
//		w, h - width and height of the window in pixels.
void resizeWindow(int w, int h)
{
	double scale, center;
	double windowXmin, windowXmax, windowYmin, windowYmax;


	// Define the portion of the window used for OpenGL rendering.
	glViewport( 0, 0, w, h );	// View port uses whole window

	// Set up the projection view matrix: orthographic projection
	// Determine the min and max values for x and y that should appear in the window.
	// The complication is that the aspect ratio of the window may not match the
	//		aspect ratio of the scene we want to view.
	w = (w==0) ? 1 : w;
	h = (h==0) ? 1 : h;
	if ( (Xmax-Xmin)/w < (Ymax-Ymin)/h ) {
		scale = ((Ymax-Ymin)/h)/((Xmax-Xmin)/w);
		center = (Xmax+Xmin)/2;
		windowXmin = center - (center-Xmin)*scale;
		windowXmax = center + (Xmax-center)*scale;
		windowYmin = Ymin;
		windowYmax = Ymax;
	}
	else {
		scale = ((Xmax-Xmin)/w)/((Ymax-Ymin)/h);
		center = (Ymax+Ymin)/2;
		windowYmin = center - (center-Ymin)*scale;
		windowYmax = center + (Ymax-center)*scale;
		windowXmin = Xmin;
		windowXmax = Xmax;
	}
	
	// Now that we know the max & min values for x & y that should be visible in the window,
	//		we set up the orthographic projection.
	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity();
	glOrtho( windowXmin, windowXmax, windowYmin, windowYmax, -1, 1 );

}

bool RestartGraph(const char *zaglavie, void (*idle)(void), void (*display)(void))
{
   if (DisplayParameters.FullScreen) 
      if (ChangeResolution(DisplayParameters)) 
          return 1;
   
   glutDestroyWindow(glutGetWindow());
   initgraph(zaglavie);
   glutIdleFunc(idle);
   glutDisplayFunc(display);
   glutReshapeFunc(resizeWindow);
   
return 0;
}

void SmoothGraphics(bool yes)
{
    // The following commands should induce OpenGL to create round points and 
	//	antialias points and lines.  (This is implementation dependent unfortunately).
    if (!yes) return;
    
	glEnable(GL_POINT_SMOOTH);
	glEnable(GL_LINE_SMOOTH);
	glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);	// Make round points, not square points
	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);		// Antialias the lines
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void putpixel(double x, double y, double z, bool no_begin)
{ 
 double wx=transformX(x), wy=transformY(y);
 
  if (!no_begin) glBegin(GL_POINTS);
  glVertex3f(wx, wy, z);
  if (!no_begin) glEnd();
}

void line(double x1, double y1, double x2, double y2, double z, bool no_begin)
{
double wx, wy, wx2, wy2;

  if (!no_begin) glBegin(GL_LINES); //GL_LINE_STRIP, GL_LINE_LOOP
  
  wx=transformX(x1);
  wy=transformY(y1);
  wx2=transformX(x2);
  wy2=transformY(y2);
  
  glVertex3f(wx, wy, z);  
  glVertex3f(wx2, wy2, z);
 
  if (!no_begin) glEnd();
}

unsigned BuildFont(const char *WindowTitle, char *FontName, int size, int weight, bool italic, bool underline, bool strikeout)
{
    unsigned FontLists;                             // Base Display List For The Font Set
    HWND    hWnd=NULL;
    HDC		hDC=NULL;
	HFONT	font;									// Windows Font ID
	HFONT	oldfont;								// Used For Good House Keeping

	FontLists = glGenLists(256);					// Storage For 96 Characters

	font = CreateFont(	-size,						// Height Of Font
						0,							// Width Of Font
						0,							// Angle Of Escapement
						0,							// Orientation Angle
						weight,						// BOLD, Normal, etc...
						italic,						// Italic
						underline,					// Underline
						strikeout,					// Strikeout
						CharacterSet,			// Character Set Identifier
						OUT_DEFAULT_PRECIS,			// Output Precision
						CLIP_DEFAULT_PRECIS,		// Clipping Precision
						ANTIALIASED_QUALITY,		// Output Quality
						FF_DONTCARE|DEFAULT_PITCH,	// Family And Pitch
						FontName);					// Font Name
						
    hWnd=FindWindow(NULL, WindowTitle);         //Select Desired OpenGL window
    hDC=GetDC(hWnd);                            

	oldfont = (HFONT)SelectObject(hDC, font);   // Selects The Font We Want
	wglUseFontBitmaps(hDC, 0, 256, FontLists);  // Builds 256 chars starting from 0
	SelectObject(hDC, oldfont);
	DeleteObject(font);							// Delete The Font
	
	return FontLists;
}

void KillFont(unsigned font)		   					// Delete The Font List
{
	glDeleteLists(font, 256);
}

void OutTextXY(double x, double y, double z, unsigned font, const char *fmt, ...)
{	
	if (fmt == NULL)									// If There's No Text
		return;											// Do Nothing

	char		text[4096];								// Holds Our String
	va_list		ap;										// Pointer To List Of Arguments

	va_start(ap, fmt);									// Parses The String For Variables
	    vsprintf(text, fmt, ap);						// And Converts Symbols To Actual Numbers
	va_end(ap);											// Results Are Stored In Text

	glPushAttrib(GL_LIST_BIT);							// Pushes The Display List Bits
	glRasterPos3f(transformX(x), transformY(y), z);     // Move to the desired position
	glListBase(font);								// Sets The Base Character to the first one
	glCallLists(strlen(text), GL_UNSIGNED_BYTE, text);	// Draws The Display List Text
	glPopAttrib();									// Pops The Display List Bits
}

void rectangle(double x1, double y1, double x2, double y2, double z, int fill)
{
double wx, wy, wx2, wy2;  

            if (!fill)   glBegin(GL_LINE_LOOP);
       else if (fill==1) glBegin(GL_POLYGON);
  
       wx=transformX(x1);
       wy=transformY(y1);
       wx2=transformX(x2);
       wy2=transformY(y2);
       
       glVertex3f(wx, wy, z);
       glVertex3f(wx, wy2, z);
       glVertex3f(wx2, wy2, z); 
       glVertex3f(wx2, wy, z);
     
  if (fill>=0) glEnd();
}

void circle(double x, double y, double z, double radius, bool fill)
{
double i,X,Y,wx,wy,step;

step=(DisplayParameters.x-radius)/10000.0;
if (step<=0) step=0.001;

if (!fill) glBegin(GL_LINE_LOOP);
else       glBegin(GL_POLYGON);

for (i=0; i<M_2PI; i+=step) 
    {
     X = x + radius * cos(i);
     Y = y + radius * sin(i);
    
     wx=transformX(X);
     wy=transformY(Y);
     glVertex3f(wx, wy, z);
    }
    
glEnd();
}

/*void ellipse(double x, double y, double z, double a, double b, bool fill)
{
double i,X,Y,wx,wy,step;

if (a<b) step=DisplayParameters.x-a;
else     step=DisplayParameters.x-b;
step/=10000.0;

if (step<=0) step=0.001;

if (!fill) glBegin(GL_LINE_LOOP);
else       glBegin(GL_POLYGON);

for (i=0; i<M_2PI; i+=step) 
    {
     X = x + a * cos(i);
     Y = y + b * sin(i);
    
     wx=DisplayParameters.x_coef*X/DisplayParameters.x-0.5;
     wy=DisplayParameters.y_coef*(DisplayParameters.y-Y)/DisplayParameters.y;
     glVertex3f(wx, wy, z);
    }
    
glEnd();  

}*/