#pragma once

#include "vzriv.h"

class __gadini
{
private:
	unsigned char i, j, korab4_napln[128];
	ship::coords stari_koordinati, poziciq_rajdane;
	float stara_krv;
	struct _vreme { double napred, zavoi, smqna_na_posoka, strelba_nova; };

public:
	bool rajdane, bez_entelekt;
	unsigned char jivi, rodeni, rajdane_lim;
	unsigned maxy, maxx, max;
	double radius[5],razmer[5],
		timeout_napred[5],timeout_zavoi[5],timeout_smqna_na_posoka[5],timeout_strelba,
		vreme_rajdane,timeout_rajdane, vreme_rajdane_poziciq,timeout_rajdane_poziciq, dai_jivot;

	ship::coords ubit;

  struct __gad
  {
		ship::coords koordinati;
		_vreme vreme;
		
		double 
			smqna_na_posoka, // ������� ���� �� ����������� �� �����
			timeout_nova_strelba;
		float 
			krv, toczki, stpka_napred, stpka_zavoi;
		char
			tip,
			smqna_lqvo, // 0 = ����� �����, 1 = ��� �����, 2 = �������
			stop;
  } *gad;
  
__gadini(int maxEnemies)
{
	gad = new __gad [maxEnemies+1];
	init(maxEnemies);
}

void init(int maxEnemies)
{
	bez_entelekt = false;
	ubit.posoka=0;
	dai_jivot=10000.0;
	rajdane=1;
	toczki=0.0;
	max=maxEnemies;
	rodeni=0;
	jivi=0;
	for (i=127; i>0; i--) korab4_napln[i]=(random(256) & 0xFF);
	
	gad[max].krv = 1; // ���������� �, �� �� ���� �������� �� �� ����� � ����� �����

	poziciq_rajdane.x=random(DisplayParameters.x);
	poziciq_rajdane.y=random(DisplayParameters.y);
}

void RodiGadini(double Cdelay, bool rodi_1)
{
	if (Cdelay - this->vreme_rajdane < this->timeout_rajdane) 
		return;
	else
		this->vreme_rajdane = Cdelay;

	if (Cdelay - this->vreme_rajdane_poziciq >= this->timeout_rajdane_poziciq) 
	{
		this->vreme_rajdane_poziciq = Cdelay;
		this->poziciq_rajdane.x = random(maxx);
		this->poziciq_rajdane.y = random(maxy);
	}

	if (this->rodeni >= this->max || this->rodeni >= nivo + 4)
	{
		this->rajdane=0;
		return;
	}
	
	int index_mrtvec = -1;
	for (this->i = 0; this->i < this->max; this->i++)
	{
		if (this->gad[i].krv <= 0)
		{
			index_mrtvec = this->i;
			break;
		}
	}
	
	if (index_mrtvec < 0)
	{
		this->rajdane = 0;
		return;
	}
	
	this->gad[index_mrtvec].timeout_nova_strelba = random(this->timeout_strelba);
	this->gad[index_mrtvec].vreme.napred = Cdelay;
	this->gad[index_mrtvec].vreme.zavoi = Cdelay;
	this->gad[index_mrtvec].vreme.strelba_nova = Cdelay;
	
	this->gad[index_mrtvec].koordinati.x=this->poziciq_rajdane.x;
	this->gad[index_mrtvec].koordinati.y=this->poziciq_rajdane.y;
	this->gad[index_mrtvec].koordinati.posoka=random(31415)/10000.0;
	
	this->gad[index_mrtvec].tip=random(5);
	this->gad[index_mrtvec].krv = 1.0;
	this->gad[index_mrtvec].stop = 0;
	this->gad[index_mrtvec].smqna_na_posoka = -1;

	switch (this->gad[index_mrtvec].tip)
	{
		case 0:
			this->gad[index_mrtvec].toczki=20;
			this->gad[index_mrtvec].stpka_napred = maxy / 200.0;
			this->gad[index_mrtvec].stpka_zavoi = 0.08;
			break;
		case 1:
			this->gad[index_mrtvec].toczki=50;
			this->gad[index_mrtvec].stpka_napred = maxy / 400.0;
			this->gad[index_mrtvec].stpka_zavoi = 0.03;
			break;
		case 2:
			this->gad[index_mrtvec].toczki=70;
			this->gad[index_mrtvec].stpka_napred = maxy / 325.0;
			this->gad[index_mrtvec].stpka_zavoi = 0.06;
			break;
		case 3:
			this->gad[index_mrtvec].toczki=100;
			this->gad[index_mrtvec].stpka_napred = maxy / 275.0;
			this->gad[index_mrtvec].stpka_zavoi = 0.04;
			break;
		case 4:
			this->gad[index_mrtvec].toczki=90;
			this->gad[index_mrtvec].stpka_napred = maxy / 400.0;
			this->gad[index_mrtvec].stpka_zavoi = 0.027;
			break;
	}
	
	this->jivi++;
	this->rodeni++;
  
	if (rodi_1) 
		this->rajdane=0;
}

void push(ship::coords novi)
{ 
  stari_koordinati=gad[0].koordinati; 
  gad[0].koordinati=novi; 
  razmer[1]*=0.5; 
  stara_krv=gad[0].krv;
  gad[0].krv=1.0;
  }

void pop()
{ gad[0].koordinati=stari_koordinati; razmer[1]*=2.0; gad[0].krv=stara_krv;}

void obraz()
{
 glEnable(GL_LINE_SMOOTH);
 glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
 glEnable(GL_BLEND);
 glLineWidth(1);
  
 for (unsigned int i=0; i<max; i++)
      {
      if (gad[i].krv <= 0) continue;
      switch (gad[i].tip)
         {
         case 0: obraz1(i);break;
         case 1: obraz2(i);break;
         case 2: obraz3(i);break;
         case 3: obraz4(i);break;
         case 4: obraz5(i);break;
         }
      }
}
void obraz5(int koi)
{
/*glColor3f( 0.5, 0.5, 0.5 );
 glDisable(GL_POINT_SMOOTH);
 glDisable(GL_LINE_SMOOTH);
 glDisable(GL_BLEND);
 glLineWidth(1);

 rectangle(gad[koi].koordinati.x-radius[4], gad[koi].koordinati.y-radius[4], gad[koi].koordinati.x+radius[4], gad[koi].koordinati.y+radius[4], -0.5 ,0);
 line(gad[koi].koordinati.x+radius[4],gad[koi].koordinati.y,gad[koi].koordinati.x-radius[4],gad[koi].koordinati.y,-0.5,0);
 line(gad[koi].koordinati.x,gad[koi].koordinati.y+radius[4],gad[koi].koordinati.x,gad[koi].koordinati.y-radius[4],-0.5,0);
 circle(gad[koi].koordinati.x, gad[koi].koordinati.y, 0, radius[4], 0);
 */
 double t[68];
 //���
 t[0]=gad[koi].koordinati.x + (razmer[4]/21.0)*cos(gad[koi].koordinati.posoka+M_PI*0.02);
 t[1]=gad[koi].koordinati.y - (razmer[4]/21.0)*sin(gad[koi].koordinati.posoka+M_PI*0.02);
 t[2]=gad[koi].koordinati.x + (razmer[4]/21.0)*cos(gad[koi].koordinati.posoka+M_PI*1.98);
 t[3]=gad[koi].koordinati.y - (razmer[4]/21.0)*sin(gad[koi].koordinati.posoka+M_PI*1.98);
 //�����
 t[4]=gad[koi].koordinati.x + (razmer[4]/70.0)*cos(gad[koi].koordinati.posoka+M_PI*0.7);
 t[5]=gad[koi].koordinati.y - (razmer[4]/70.0)*sin(gad[koi].koordinati.posoka+M_PI*0.7);
 t[6]=gad[koi].koordinati.x + (razmer[4]/70.0)*cos(gad[koi].koordinati.posoka+M_PI*1.3);
 t[7]=gad[koi].koordinati.y - (razmer[4]/70.0)*sin(gad[koi].koordinati.posoka+M_PI*1.3);
 //����� ����������� - ������
 t[8]=gad[koi].koordinati.x + (razmer[4]/45.0)*cos(gad[koi].koordinati.posoka+M_PI*0.675);
 t[9]=gad[koi].koordinati.y - (razmer[4]/45.0)*sin(gad[koi].koordinati.posoka+M_PI*0.675);
 t[10]=gad[koi].koordinati.x + (razmer[4]/45.0)*cos(gad[koi].koordinati.posoka+M_PI*1.325);
 t[11]=gad[koi].koordinati.y - (razmer[4]/45.0)*sin(gad[koi].koordinati.posoka+M_PI*1.325);
 //����� ����������� - ������
 t[12]=gad[koi].koordinati.x + (razmer[4]/42.0)*cos(gad[koi].koordinati.posoka+M_PI*0.6);
 t[13]=gad[koi].koordinati.y - (razmer[4]/42.0)*sin(gad[koi].koordinati.posoka+M_PI*0.6);
 t[14]=gad[koi].koordinati.x + (razmer[4]/42.0)*cos(gad[koi].koordinati.posoka+M_PI*1.4);
 t[15]=gad[koi].koordinati.y - (razmer[4]/42.0)*sin(gad[koi].koordinati.posoka+M_PI*1.4);
 //����� - �����
 t[16]=gad[koi].koordinati.x + (razmer[4]/25.5)*cos(gad[koi].koordinati.posoka+M_PI*0.19);
 t[17]=gad[koi].koordinati.y - (razmer[4]/25.5)*sin(gad[koi].koordinati.posoka+M_PI*0.19);
 t[18]=gad[koi].koordinati.x + (razmer[4]/25.5)*cos(gad[koi].koordinati.posoka+M_PI*1.81);
 t[19]=gad[koi].koordinati.y - (razmer[4]/25.5)*sin(gad[koi].koordinati.posoka+M_PI*1.81);
 //����� - ����
 t[20]=gad[koi].koordinati.x + (razmer[4]/22.0)*cos(gad[koi].koordinati.posoka+M_PI*0.25);
 t[21]=gad[koi].koordinati.y - (razmer[4]/22.0)*sin(gad[koi].koordinati.posoka+M_PI*0.25);
 t[22]=gad[koi].koordinati.x + (razmer[4]/22.0)*cos(gad[koi].koordinati.posoka+M_PI*1.75);
 t[23]=gad[koi].koordinati.y - (razmer[4]/22.0)*sin(gad[koi].koordinati.posoka+M_PI*1.75);
 //����� - ���
 t[24]=gad[koi].koordinati.x + (razmer[4]/20.0)*cos(gad[koi].koordinati.posoka+M_PI*0.775);
 t[25]=gad[koi].koordinati.y - (razmer[4]/20.0)*sin(gad[koi].koordinati.posoka+M_PI*0.775);
 t[26]=gad[koi].koordinati.x + (razmer[4]/20.0)*cos(gad[koi].koordinati.posoka+M_PI*1.225);
 t[27]=gad[koi].koordinati.y - (razmer[4]/20.0)*sin(gad[koi].koordinati.posoka+M_PI*1.225);
 //����� - ����� - ����
 t[28]=gad[koi].koordinati.x + (razmer[4]/22.5)*cos(gad[koi].koordinati.posoka+M_PI*0.83);
 t[29]=gad[koi].koordinati.y - (razmer[4]/22.5)*sin(gad[koi].koordinati.posoka+M_PI*0.83);
 t[30]=gad[koi].koordinati.x + (razmer[4]/22.5)*cos(gad[koi].koordinati.posoka+M_PI*1.17);
 t[31]=gad[koi].koordinati.y - (razmer[4]/22.5)*sin(gad[koi].koordinati.posoka+M_PI*1.17);
 //����� - ����� - �����
 t[32]=gad[koi].koordinati.x + (razmer[4]/32.5)*cos(gad[koi].koordinati.posoka+M_PI*0.74);
 t[33]=gad[koi].koordinati.y - (razmer[4]/32.5)*sin(gad[koi].koordinati.posoka+M_PI*0.74);
 t[34]=gad[koi].koordinati.x + (razmer[4]/32.5)*cos(gad[koi].koordinati.posoka+M_PI*1.26);
 t[35]=gad[koi].koordinati.y - (razmer[4]/32.5)*sin(gad[koi].koordinati.posoka+M_PI*1.26);
 //����� �����������
 t[36]=gad[koi].koordinati.x + (razmer[4]/53.0)*cos(gad[koi].koordinati.posoka+M_PI*0.8);
 t[37]=gad[koi].koordinati.y - (razmer[4]/53.0)*sin(gad[koi].koordinati.posoka+M_PI*0.8);
 t[38]=gad[koi].koordinati.x + (razmer[4]/53.0)*cos(gad[koi].koordinati.posoka+M_PI*1.2);
 t[39]=gad[koi].koordinati.y - (razmer[4]/53.0)*sin(gad[koi].koordinati.posoka+M_PI*1.2);
 //�������
 t[40]=gad[koi].koordinati.x + (razmer[4]/38.0)*cos(gad[koi].koordinati.posoka+M_PI*0.95);
 t[41]=gad[koi].koordinati.y - (razmer[4]/38.0)*sin(gad[koi].koordinati.posoka+M_PI*0.95);
 t[42]=gad[koi].koordinati.x + (razmer[4]/38.0)*cos(gad[koi].koordinati.posoka+M_PI*1.05);
 t[43]=gad[koi].koordinati.y - (razmer[4]/38.0)*sin(gad[koi].koordinati.posoka+M_PI*1.05);
 //���������
 //������ ������
 t[44]=gad[koi].koordinati.x + (razmer[4]/180.0)*cos(gad[koi].koordinati.posoka+M_PI*0.45);
 t[45]=gad[koi].koordinati.y - (razmer[4]/180.0)*sin(gad[koi].koordinati.posoka+M_PI*0.45);
 t[46]=gad[koi].koordinati.x + (razmer[4]/180.0)*cos(gad[koi].koordinati.posoka+M_PI*1.55);
 t[47]=gad[koi].koordinati.y - (razmer[4]/180.0)*sin(gad[koi].koordinati.posoka+M_PI*1.55);
 t[48]=gad[koi].koordinati.x + (razmer[4]/23.0)*cos(gad[koi].koordinati.posoka);
 t[49]=gad[koi].koordinati.y - (razmer[4]/23.0)*sin(gad[koi].koordinati.posoka);
 //����� ������
 t[50]=gad[koi].koordinati.x + (razmer[4]/150.0)*cos(gad[koi].koordinati.posoka+M_PI*0.625);
 t[51]=gad[koi].koordinati.y - (razmer[4]/150.0)*sin(gad[koi].koordinati.posoka+M_PI*0.625);
 t[52]=gad[koi].koordinati.x + (razmer[4]/150.0)*cos(gad[koi].koordinati.posoka+M_PI*1.375);
 t[53]=gad[koi].koordinati.y - (razmer[4]/150.0)*sin(gad[koi].koordinati.posoka+M_PI*1.375);

 t[54]=gad[koi].koordinati.x + (razmer[4]/65.0)*cos(gad[koi].koordinati.posoka+M_PI*0.9);
 t[55]=gad[koi].koordinati.y - (razmer[4]/65.0)*sin(gad[koi].koordinati.posoka+M_PI*0.9);
 t[56]=gad[koi].koordinati.x + (razmer[4]/65.0)*cos(gad[koi].koordinati.posoka+M_PI*1.1);
 t[57]=gad[koi].koordinati.y - (razmer[4]/65.0)*sin(gad[koi].koordinati.posoka+M_PI*1.1);

 t[58]=gad[koi].koordinati.x + (razmer[4]/55.0)*cos(gad[koi].koordinati.posoka+M_PI);
 t[59]=gad[koi].koordinati.y - (razmer[4]/55.0)*sin(gad[koi].koordinati.posoka+M_PI);
 
 //�����
 t[60]=gad[koi].koordinati.x + (razmer[4]/22.3)*cos(gad[koi].koordinati.posoka+M_PI*0.79);
 t[61]=gad[koi].koordinati.y - (razmer[4]/22.3)*sin(gad[koi].koordinati.posoka+M_PI*0.79);
 t[62]=gad[koi].koordinati.x + (razmer[4]/22.3)*cos(gad[koi].koordinati.posoka+M_PI*1.21);
 t[63]=gad[koi].koordinati.y - (razmer[4]/22.3)*sin(gad[koi].koordinati.posoka+M_PI*1.21);

 t[64]=gad[koi].koordinati.x + (razmer[4]/25.0)*cos(gad[koi].koordinati.posoka+M_PI*0.24);
 t[65]=gad[koi].koordinati.y - (razmer[4]/25.0)*sin(gad[koi].koordinati.posoka+M_PI*0.24);
 t[66]=gad[koi].koordinati.x + (razmer[4]/25.0)*cos(gad[koi].koordinati.posoka+M_PI*1.76);
 t[67]=gad[koi].koordinati.y - (razmer[4]/25.0)*sin(gad[koi].koordinati.posoka+M_PI*1.76);

 glColor3f(0.9,0.9,0.9);
 glBegin(GL_TRIANGLE_STRIP);
 //���� �����
 glVertex3f(transformX(t[16]),transformY(t[17]),0);
 glVertex3f(transformX(t[20]),transformY(t[21]),0);
 glVertex3f(transformX(t[28]),transformY(t[29]),0);
 glVertex3f(transformX(t[24]),transformY(t[25]),0);
 //���� �����������
 glVertex3f(transformX(t[12]),transformY(t[13]),0);
 glVertex3f(transformX(t[32]),transformY(t[33]),0);
 glVertex3f(transformX(t[8]),transformY(t[9]),0);
 glVertex3f(transformX(t[36]),transformY(t[37]),0);
 glVertex3f(transformX(t[4]),transformY(t[5]),0);
 //����
 glVertex3f(transformX(t[0]),transformY(t[1]),0);
 glVertex3f(transformX(t[36]),transformY(t[37]),0);
 glVertex3f(transformX(t[2]),transformY(t[3]),0);
 glVertex3f(transformX(t[40]),transformY(t[41]),0);
 glVertex3f(transformX(t[6]),transformY(t[7]),0);
 glVertex3f(transformX(t[42]),transformY(t[43]),0);
 glVertex3f(transformX(t[38]),transformY(t[39]),0);
 //����� �����������
 glVertex3f(transformX(t[6]),transformY(t[7]),0);
 glVertex3f(transformX(t[34]),transformY(t[35]),0);
 glVertex3f(transformX(t[10]),transformY(t[11]),0);
 glVertex3f(transformX(t[14]),transformY(t[15]),0);
 //����� �����
 glVertex3f(transformX(t[26]),transformY(t[27]),0);
 glVertex3f(transformX(t[30]),transformY(t[31]),0);
 glVertex3f(transformX(t[22]),transformY(t[23]),0);
 glVertex3f(transformX(t[18]),transformY(t[19]),0);
 glEnd();
 
 glColor3f(1-gad[koi].krv, 0.4, gad[koi].krv);
 glBegin(GL_TRIANGLES);
 glVertex3f(transformX(t[44]),transformY(t[45]),0.01);
 glVertex3f(transformX(t[46]),transformY(t[47]),0.01);
 glVertex3f(transformX(t[48]),transformY(t[49]),0.01);
 glEnd();
 
 glColor3f(0,0.4,1);
 glBegin(GL_TRIANGLE_STRIP);
 glVertex3f(transformX(t[50]),transformY(t[51]),0.01);
 glVertex3f(transformX(t[52]),transformY(t[53]),0.01);
 glVertex3f(transformX(t[54]),transformY(t[55]),0.01);
 glVertex3f(transformX(t[56]),transformY(t[57]),0.01);
 glVertex3f(transformX(t[58]),transformY(t[59]),0.01);
 glEnd();
 
 glColor3f(0.7, 0.7, 0.7); 
 glBegin(GL_LINES);
 //���
 line(t[0],t[1],t[2],t[3],0.02,1);
 //�����
 line(t[0],t[1],t[4],t[5],0.02,1);
 line(t[2],t[3],t[6],t[7],0.02,1);
 //����� ����������� - ������
 line(t[4],t[5],t[8],t[9],0.02,1);
 line(t[6],t[7],t[10],t[11],0.02,1);
 //����� ����������� - ������
 line(t[8],t[9],t[12],t[13],0.02,1);
 line(t[10],t[11],t[14],t[15],0.02,1);
 //����� - �����
 line(t[12],t[13],t[16],t[17],0.02,1);
 line(t[14],t[15],t[18],t[19],0.02,1);
 //����� - ����
 line(t[16],t[17],t[20],t[21],0.02,1);
 line(t[18],t[19],t[22],t[23],0.02,1);
 //����� - ����
 line(t[20],t[21],t[24],t[25],0.02,1);
 line(t[22],t[23],t[26],t[27],0.02,1);
 //����� - ����� - ����
 line(t[24],t[25],t[28],t[29],0.02,1);
 line(t[26],t[27],t[30],t[31],0.02,1);
 //����� - ����� - �����
 line(t[28],t[29],t[32],t[33],0.02,1);
 line(t[30],t[31],t[34],t[35],0.02,1);
 //����� �����������
 line(t[32],t[33],t[36],t[37],0.02,1);
 line(t[34],t[35],t[38],t[39],0.02,1);
 //�������
 line(t[36],t[37],t[40],t[41],0.02,1);
 line(t[38],t[39],t[42],t[43],0.02,1);
 line(t[40],t[41],t[42],t[43],0.02,1);
 //���������
 glColor3f(0, 0.33, 0.5);
 //������ ������
 line(t[44],t[45],t[46],t[47],0.02,1);
 line(t[48],t[49],t[46],t[47],0.02,1);
 line(t[48],t[49],t[44],t[45],0.02,1);
 //����� ������
 line(t[50],t[51],t[52],t[53],0.02,1);
 line(t[50],t[51],t[54],t[55],0.02,1);
 line(t[52],t[53],t[56],t[57],0.02,1);
 line(t[58],t[59],t[54],t[55],0.02,1);
 line(t[58],t[59],t[56],t[57],0.02,1);
 glColor3f(1,0,0);
 //�����
 line(t[60],t[61],t[64],t[65],0.02,1);
 line(t[62],t[63],t[66],t[67],0.02,1);
 glEnd();
}

void obraz4(int koi)
{
/* glColor3f( 0.5, 0.5, 0.5 );
 glDisable(GL_POINT_SMOOTH);
 glDisable(GL_LINE_SMOOTH);
 glDisable(GL_BLEND);
 glLineWidth(1);

 rectangle(gad[koi].koordinati.x-radius[3], gad[koi].koordinati.y-radius[3], gad[koi].koordinati.x+radius[3], gad[koi].koordinati.y+radius[3], -0.5 ,0);
 line(gad[koi].koordinati.x+radius[3],gad[koi].koordinati.y,gad[koi].koordinati.x-radius[3],gad[koi].koordinati.y,-0.5,0);
 line(gad[koi].koordinati.x,gad[koi].koordinati.y+radius[3],gad[koi].koordinati.x,gad[koi].koordinati.y-radius[3],-0.5,0);
 circle(gad[koi].koordinati.x, gad[koi].koordinati.y, 0, radius[3], 0);
*/
 double t[56];
 
 //�����������
 t[0]=gad[koi].koordinati.x + (razmer[3]/300.0)*cos(gad[koi].koordinati.posoka);
 t[1]=gad[koi].koordinati.y - (razmer[3]/300.0)*sin(gad[koi].koordinati.posoka);
 t[2]=gad[koi].koordinati.x + (razmer[3]/100.0)*cos(gad[koi].koordinati.posoka+M_PI*0.15);
 t[3]=gad[koi].koordinati.y - (razmer[3]/100.0)*sin(gad[koi].koordinati.posoka+M_PI*0.15);
 t[4]=gad[koi].koordinati.x + (razmer[3]/100.0)*cos(gad[koi].koordinati.posoka+M_PI*1.85);
 t[5]=gad[koi].koordinati.y - (razmer[3]/100.0)*sin(gad[koi].koordinati.posoka+M_PI*1.85);

 //����� - ����� 
 t[6]=gad[koi].koordinati.x + (razmer[3]/20.0)*cos(gad[koi].koordinati.posoka+M_PI*0.05);
 t[7]=gad[koi].koordinati.y - (razmer[3]/20.0)*sin(gad[koi].koordinati.posoka+M_PI*0.05);
 t[8]=gad[koi].koordinati.x + (razmer[3]/20.0)*cos(gad[koi].koordinati.posoka+M_PI*1.95);
 t[9]=gad[koi].koordinati.y - (razmer[3]/20.0)*sin(gad[koi].koordinati.posoka+M_PI*1.95);
 //����� - ����
 t[10]=gad[koi].koordinati.x + (razmer[3]/23.5)*cos(gad[koi].koordinati.posoka+M_PI*0.12);
 t[11]=gad[koi].koordinati.y - (razmer[3]/23.5)*sin(gad[koi].koordinati.posoka+M_PI*0.12);
 t[12]=gad[koi].koordinati.x + (razmer[3]/23.5)*cos(gad[koi].koordinati.posoka+M_PI*1.88);
 t[13]=gad[koi].koordinati.y - (razmer[3]/23.5)*sin(gad[koi].koordinati.posoka+M_PI*1.88);
 //����� - �����1
 t[14]=gad[koi].koordinati.x + (razmer[3]/40.0)*cos(gad[koi].koordinati.posoka+M_PI*0.45);
 t[15]=gad[koi].koordinati.y - (razmer[3]/40.0)*sin(gad[koi].koordinati.posoka+M_PI*0.45);
 t[16]=gad[koi].koordinati.x + (razmer[3]/40.0)*cos(gad[koi].koordinati.posoka+M_PI*1.55);
 t[17]=gad[koi].koordinati.y - (razmer[3]/40.0)*sin(gad[koi].koordinati.posoka+M_PI*1.55);
 //����� - �����2
 t[18]=gad[koi].koordinati.x + (razmer[3]/24.25)*cos(gad[koi].koordinati.posoka+M_PI*0.8);
 t[19]=gad[koi].koordinati.y - (razmer[3]/24.25)*sin(gad[koi].koordinati.posoka+M_PI*0.8);
 t[20]=gad[koi].koordinati.x + (razmer[3]/24.25)*cos(gad[koi].koordinati.posoka+M_PI*1.2);
 t[21]=gad[koi].koordinati.y - (razmer[3]/24.25)*sin(gad[koi].koordinati.posoka+M_PI*1.2);
 //����� - �����
 t[22]=gad[koi].koordinati.x + (razmer[3]/22.25)*cos(gad[koi].koordinati.posoka+M_PI*0.89);
 t[23]=gad[koi].koordinati.y - (razmer[3]/22.25)*sin(gad[koi].koordinati.posoka+M_PI*0.89);
 t[24]=gad[koi].koordinati.x + (razmer[3]/22.25)*cos(gad[koi].koordinati.posoka+M_PI*1.11);
 t[25]=gad[koi].koordinati.y - (razmer[3]/22.25)*sin(gad[koi].koordinati.posoka+M_PI*1.11);
 //������
 t[26]=gad[koi].koordinati.x + (razmer[3]/20.5)*cos(gad[koi].koordinati.posoka+M_PI*0.98);
 t[27]=gad[koi].koordinati.y - (razmer[3]/20.5)*sin(gad[koi].koordinati.posoka+M_PI*0.98);
 t[28]=gad[koi].koordinati.x + (razmer[3]/20.5)*cos(gad[koi].koordinati.posoka+M_PI*1.02);
 t[29]=gad[koi].koordinati.y - (razmer[3]/20.5)*sin(gad[koi].koordinati.posoka+M_PI*1.02);
 
 //���������
 //������
 t[30]=gad[koi].koordinati.x + (razmer[3]/27.0)*cos(gad[koi].koordinati.posoka+M_PI*0.92);
 t[31]=gad[koi].koordinati.y - (razmer[3]/27.0)*sin(gad[koi].koordinati.posoka+M_PI*0.92);
 t[32]=gad[koi].koordinati.x + (razmer[3]/27.0)*cos(gad[koi].koordinati.posoka+M_PI*1.08);
 t[33]=gad[koi].koordinati.y - (razmer[3]/27.0)*sin(gad[koi].koordinati.posoka+M_PI*1.08);
 
 t[34]=gad[koi].koordinati.x + (razmer[3]/80.0)*cos(gad[koi].koordinati.posoka+M_PI);
 t[35]=gad[koi].koordinati.y - (razmer[3]/80.0)*sin(gad[koi].koordinati.posoka+M_PI);
 t[36]=gad[koi].koordinati.x + (razmer[3]/37.0)*cos(gad[koi].koordinati.posoka+M_PI);
 t[37]=gad[koi].koordinati.y - (razmer[3]/37.0)*sin(gad[koi].koordinati.posoka+M_PI);
 //�����
 t[38]=gad[koi].koordinati.x + (razmer[3]/25.0)*cos(gad[koi].koordinati.posoka+M_PI*0.09);
 t[39]=gad[koi].koordinati.y - (razmer[3]/25.0)*sin(gad[koi].koordinati.posoka+M_PI*0.09);
 t[40]=gad[koi].koordinati.x + (razmer[3]/25.0)*cos(gad[koi].koordinati.posoka+M_PI*1.91);
 t[41]=gad[koi].koordinati.y - (razmer[3]/25.0)*sin(gad[koi].koordinati.posoka+M_PI*1.91);

 t[42]=gad[koi].koordinati.x + (razmer[3]/28.5)*cos(gad[koi].koordinati.posoka+M_PI*0.81);
 t[43]=gad[koi].koordinati.y - (razmer[3]/28.5)*sin(gad[koi].koordinati.posoka+M_PI*0.81);
 t[44]=gad[koi].koordinati.x + (razmer[3]/28.5)*cos(gad[koi].koordinati.posoka+M_PI*1.19);
 t[45]=gad[koi].koordinati.y - (razmer[3]/28.5)*sin(gad[koi].koordinati.posoka+M_PI*1.19);

 t[46]=gad[koi].koordinati.x + (razmer[3]/42.0)*cos(gad[koi].koordinati.posoka+M_PI*0.93);
 t[47]=gad[koi].koordinati.y - (razmer[3]/42.0)*sin(gad[koi].koordinati.posoka+M_PI*0.93);
 t[48]=gad[koi].koordinati.x + (razmer[3]/42.0)*cos(gad[koi].koordinati.posoka+M_PI*1.07);
 t[49]=gad[koi].koordinati.y - (razmer[3]/42.0)*sin(gad[koi].koordinati.posoka+M_PI*1.07);
  
 glColor3f(0.25, 0.25, 0.25);
 glEnable(GL_POLYGON_STIPPLE);
 glPolygonStipple(korab4_napln);
 glBegin(GL_TRIANGLE_STRIP);
 //���� �����
 glVertex3f(transformX(t[10]),transformY(t[11]),0);
 glVertex3f(transformX(t[6]),transformY(t[7]),0);
 glVertex3f(transformX(t[14]),transformY(t[15]),0); 
 glVertex3f(transformX(t[2]),transformY(t[3]),0);
 glVertex3f(transformX(t[18]),transformY(t[19]),0);
 glVertex3f(transformX(t[0]),transformY(t[1]),0);
 glVertex3f(transformX(t[22]),transformY(t[23]),0);

 //����
 glVertex3f(transformX(t[4]),transformY(t[5]),0);
 glVertex3f(transformX(t[26]),transformY(t[27]),0);
 glVertex3f(transformX(t[28]),transformY(t[29]),0);
 glVertex3f(transformX(t[24]),transformY(t[25]),0);

 //����� �����
 glVertex3f(transformX(t[4]),transformY(t[5]),0);
 glVertex3f(transformX(t[20]),transformY(t[21]),0);
 glVertex3f(transformX(t[8]),transformY(t[9]),0);
 glVertex3f(transformX(t[16]),transformY(t[17]),0);
 glVertex3f(transformX(t[12]),transformY(t[13]),0);
 glEnd();
 glDisable(GL_POLYGON_STIPPLE);
 
 //������
 glColor3f(0.5, 0.5*gad[koi].krv, 0.5*gad[koi].krv);
 glBegin(GL_TRIANGLES);
 glVertex3f(transformX(t[30]),transformY(t[31]),0.01);
 glVertex3f(transformX(t[32]),transformY(t[33]),0.01);
 glVertex3f(transformX(t[34]),transformY(t[35]),0.01);
 glEnd();
 
 glColor3f(0.45, 0.45, 0.45); 
 glBegin(GL_LINES);
 //�����������
 line(t[0],t[1],t[2],t[3],0.02,1);
 line(t[0],t[1],t[4],t[5],0.02,1);
 //����� - �����
 line(t[2],t[3],t[6],t[7],0.02,1);
 line(t[4],t[5],t[8],t[9],0.02,1);
 //����� - ����
 line(t[6],t[7],t[10],t[11],0.02,1);
 line(t[8],t[9],t[12],t[13],0.02,1);
 //����� - �����
 line(t[10],t[11],t[14],t[15],0.02,1);
 line(t[12],t[13],t[16],t[17],0.02,1);
 //����� - �����2
 line(t[14],t[15],t[18],t[19],0.02,1);
 line(t[16],t[17],t[20],t[21],0.02,1);
 //����� - �����
 line(t[18],t[19],t[22],t[23],0.02,1);
 line(t[20],t[21],t[24],t[25],0.02,1);
 //������
 line(t[26],t[27],t[28],t[29],0.02,1);
 line(t[24],t[25],t[28],t[29],0.02,1);
 line(t[22],t[23],t[26],t[27],0.02,1);

 
 //���������
 //������
 glColor3f(1, 1, 1); 
 line(t[30],t[31],t[32],t[33],0.02,1);
 line(t[30],t[31],t[34],t[35],0.02,1);
 line(t[32],t[33],t[34],t[35],0.02,1);
 
 line(t[30],t[31],t[36],t[37],0.02,1);
 line(t[32],t[33],t[36],t[37],0.02,1);
 line(t[34],t[35],t[36],t[37],0.02,1);
 
 glColor3f(0.66, 0.66, 0.66);
 line(t[30],t[31],t[22],t[23],0.02,1);
 line(t[32],t[33],t[24],t[25],0.02,1);
 
 //�����
 line(t[38],t[39],t[42],t[43],0.02,1);
 line(t[40],t[41],t[44],t[45],0.02,1);
 line(t[42],t[43],t[46],t[47],0.02,1);
 line(t[44],t[45],t[48],t[49],0.02,1);
 
 line(t[22],t[23],t[24],t[25],0.02,1);
 glEnd();
}

void obraz3(int koi)
{
/* glColor3f( 0.5, 0.5, 0.5 );
 glDisable(GL_POINT_SMOOTH);
 glDisable(GL_LINE_SMOOTH);
 glDisable(GL_BLEND);
 glLineWidth(1);

 rectangle(gad[koi].koordinati.x-radius[2], gad[koi].koordinati.y-radius[2], gad[koi].koordinati.x+radius[2], gad[koi].koordinati.y+radius[1], -0.5 ,0);
 line(gad[koi].koordinati.x+radius[2],gad[koi].koordinati.y,gad[koi].koordinati.x-radius[2],gad[koi].koordinati.y,-0.5,0);
 line(gad[koi].koordinati.x,gad[koi].koordinati.y+radius[2],gad[koi].koordinati.x,gad[koi].koordinati.y-radius[2],-0.5,0);
 circle(gad[koi].koordinati.x, gad[koi].koordinati.y, 0, radius[2], 0);
*/
 double t[62];
 
 //���  
 t[0]=gad[koi].koordinati.x + (razmer[2]/20.0)*cos(gad[koi].koordinati.posoka);
 t[1]=gad[koi].koordinati.y - (razmer[2]/20.0)*sin(gad[koi].koordinati.posoka);
 t[2]=gad[koi].koordinati.x + (razmer[2]/55.0)*cos(gad[koi].koordinati.posoka+M_PI*0.65);
 t[3]=gad[koi].koordinati.y - (razmer[2]/55.0)*sin(gad[koi].koordinati.posoka+M_PI*0.65);
 t[4]=gad[koi].koordinati.x + (razmer[2]/55.0)*cos(gad[koi].koordinati.posoka+M_PI*1.35);
 t[5]=gad[koi].koordinati.y - (razmer[2]/55.0)*sin(gad[koi].koordinati.posoka+M_PI*1.35);
 
 //����
 t[6]=gad[koi].koordinati.x + (razmer[2]/35.0)*cos(gad[koi].koordinati.posoka+M_PI*0.81);
 t[7]=gad[koi].koordinati.y - (razmer[2]/35.0)*sin(gad[koi].koordinati.posoka+M_PI*0.81);
 t[8]=gad[koi].koordinati.x + (razmer[2]/35.0)*cos(gad[koi].koordinati.posoka+M_PI*1.19);
 t[9]=gad[koi].koordinati.y - (razmer[2]/35.0)*sin(gad[koi].koordinati.posoka+M_PI*1.19);
 t[10]=gad[koi].koordinati.x + (razmer[2]/65.0)*cos(gad[koi].koordinati.posoka+M_PI);
 t[11]=gad[koi].koordinati.y - (razmer[2]/65.0)*sin(gad[koi].koordinati.posoka+M_PI);

 //�����
 t[12]=gad[koi].koordinati.x + (razmer[2]/30.0)*cos(gad[koi].koordinati.posoka+M_PI*0.63);
 t[13]=gad[koi].koordinati.y - (razmer[2]/30.0)*sin(gad[koi].koordinati.posoka+M_PI*0.63);
 t[14]=gad[koi].koordinati.x + (razmer[2]/30.0)*cos(gad[koi].koordinati.posoka+M_PI*1.37);
 t[15]=gad[koi].koordinati.y - (razmer[2]/30.0)*sin(gad[koi].koordinati.posoka+M_PI*1.37);

 t[16]=gad[koi].koordinati.x + (razmer[2]/32.0)*cos(gad[koi].koordinati.posoka+M_PI*0.53);
 t[17]=gad[koi].koordinati.y - (razmer[2]/32.0)*sin(gad[koi].koordinati.posoka+M_PI*0.53);
 t[18]=gad[koi].koordinati.x + (razmer[2]/32.0)*cos(gad[koi].koordinati.posoka+M_PI*1.47);
 t[19]=gad[koi].koordinati.y - (razmer[2]/32.0)*sin(gad[koi].koordinati.posoka+M_PI*1.47);
 
 t[20]=gad[koi].koordinati.x + (razmer[2]/22.5)*cos(gad[koi].koordinati.posoka+M_PI*0.52);
 t[21]=gad[koi].koordinati.y - (razmer[2]/22.5)*sin(gad[koi].koordinati.posoka+M_PI*0.52);
 t[22]=gad[koi].koordinati.x + (razmer[2]/22.5)*cos(gad[koi].koordinati.posoka+M_PI*1.48);
 t[23]=gad[koi].koordinati.y - (razmer[2]/22.5)*sin(gad[koi].koordinati.posoka+M_PI*1.48);
 
 //����� - ����� ����
 t[24]=gad[koi].koordinati.x + (razmer[2]/20.5)*cos(gad[koi].koordinati.posoka+M_PI*0.63);
 t[25]=gad[koi].koordinati.y - (razmer[2]/20.5)*sin(gad[koi].koordinati.posoka+M_PI*0.63);
 t[26]=gad[koi].koordinati.x + (razmer[2]/20.5)*cos(gad[koi].koordinati.posoka+M_PI*1.37);
 t[27]=gad[koi].koordinati.y - (razmer[2]/20.5)*sin(gad[koi].koordinati.posoka+M_PI*1.37);

 t[28]=gad[koi].koordinati.x + (razmer[2]/20.5)*cos(gad[koi].koordinati.posoka+M_PI*0.77);
 t[29]=gad[koi].koordinati.y - (razmer[2]/20.5)*sin(gad[koi].koordinati.posoka+M_PI*0.77);
 t[30]=gad[koi].koordinati.x + (razmer[2]/20.5)*cos(gad[koi].koordinati.posoka+M_PI*1.23);
 t[31]=gad[koi].koordinati.y - (razmer[2]/20.5)*sin(gad[koi].koordinati.posoka+M_PI*1.23);

 //������
 t[32]=gad[koi].koordinati.x + (razmer[2]/21.0)*cos(gad[koi].koordinati.posoka+M_PI*0.9);
 t[33]=gad[koi].koordinati.y - (razmer[2]/21.0)*sin(gad[koi].koordinati.posoka+M_PI*0.9);
 t[34]=gad[koi].koordinati.x + (razmer[2]/21.0)*cos(gad[koi].koordinati.posoka+M_PI*1.1);
 t[35]=gad[koi].koordinati.y - (razmer[2]/21.0)*sin(gad[koi].koordinati.posoka+M_PI*1.1);
 
 //��������� - ���
 t[36]=gad[koi].koordinati.x + (razmer[2]/30.0)*cos(gad[koi].koordinati.posoka);
 t[37]=gad[koi].koordinati.y - (razmer[2]/30.0)*sin(gad[koi].koordinati.posoka);
 t[38]=gad[koi].koordinati.x + (razmer[2]/300.0)*cos(gad[koi].koordinati.posoka+M_PI);
 t[39]=gad[koi].koordinati.y - (razmer[2]/300.0)*sin(gad[koi].koordinati.posoka+M_PI);

 t[40]=gad[koi].koordinati.x + (razmer[2]/60.0)*cos(gad[koi].koordinati.posoka+M_PI*0.875);
 t[41]=gad[koi].koordinati.y - (razmer[2]/60.0)*sin(gad[koi].koordinati.posoka+M_PI*0.875);
 t[42]=gad[koi].koordinati.x + (razmer[2]/60.0)*cos(gad[koi].koordinati.posoka+M_PI*1.125);
 t[43]=gad[koi].koordinati.y - (razmer[2]/60.0)*sin(gad[koi].koordinati.posoka+M_PI*1.125);

 t[44]=gad[koi].koordinati.x + (razmer[2]/75.0)*cos(gad[koi].koordinati.posoka+M_PI*0.7);
 t[45]=gad[koi].koordinati.y - (razmer[2]/75.0)*sin(gad[koi].koordinati.posoka+M_PI*0.7);
 t[46]=gad[koi].koordinati.x + (razmer[2]/75.0)*cos(gad[koi].koordinati.posoka+M_PI*1.3);
 t[47]=gad[koi].koordinati.y - (razmer[2]/75.0)*sin(gad[koi].koordinati.posoka+M_PI*1.3);
 
 //��������� - �����
 t[48]=gad[koi].koordinati.x + (razmer[2]/26.0)*cos(gad[koi].koordinati.posoka+M_PI*0.56);
 t[49]=gad[koi].koordinati.y - (razmer[2]/26.0)*sin(gad[koi].koordinati.posoka+M_PI*0.56);
 t[50]=gad[koi].koordinati.x + (razmer[2]/26.0)*cos(gad[koi].koordinati.posoka+M_PI*1.44);
 t[51]=gad[koi].koordinati.y - (razmer[2]/26.0)*sin(gad[koi].koordinati.posoka+M_PI*1.44);

 t[52]=gad[koi].koordinati.x + (razmer[2]/23.5)*cos(gad[koi].koordinati.posoka+M_PI*0.65);
 t[53]=gad[koi].koordinati.y - (razmer[2]/23.5)*sin(gad[koi].koordinati.posoka+M_PI*0.65);
 t[54]=gad[koi].koordinati.x + (razmer[2]/23.5)*cos(gad[koi].koordinati.posoka+M_PI*1.35);
 t[55]=gad[koi].koordinati.y - (razmer[2]/23.5)*sin(gad[koi].koordinati.posoka+M_PI*1.35);

 //��������� - ������
 t[56]=gad[koi].koordinati.x + (razmer[2]/25.0)*cos(gad[koi].koordinati.posoka+M_PI*0.875);
 t[57]=gad[koi].koordinati.y - (razmer[2]/25.0)*sin(gad[koi].koordinati.posoka+M_PI*0.875);
 t[58]=gad[koi].koordinati.x + (razmer[2]/25.0)*cos(gad[koi].koordinati.posoka+M_PI*1.125);
 t[59]=gad[koi].koordinati.y - (razmer[2]/25.0)*sin(gad[koi].koordinati.posoka+M_PI*1.125);
 t[60]=gad[koi].koordinati.x + (razmer[2]/41.0)*cos(gad[koi].koordinati.posoka+M_PI);
 t[61]=gad[koi].koordinati.y - (razmer[2]/41.0)*sin(gad[koi].koordinati.posoka+M_PI);

 glColor3f(0, 0.5, 0.5);
 glBegin(GL_TRIANGLE_STRIP);
 //���� �����
 glVertex3f(transformX(t[20]),transformY(t[21]),0);
 glVertex3f(transformX(t[16]),transformY(t[17]),0);
 glVertex3f(transformX(t[24]),transformY(t[25]),0);
 glVertex3f(transformX(t[28]),transformY(t[29]),0);
 glVertex3f(transformX(t[12]),transformY(t[13]),0);
 glVertex3f(transformX(t[32]),transformY(t[33]),0);
 glVertex3f(transformX(t[6]),transformY(t[7]),0);
 //�������
 glVertex3f(transformX(t[34]),transformY(t[35]),0);
 glVertex3f(transformX(t[8]),transformY(t[9]),0);
 //����� �����
 glVertex3f(transformX(t[30]),transformY(t[31]),0);
 glVertex3f(transformX(t[14]),transformY(t[15]),0);
 glVertex3f(transformX(t[26]),transformY(t[27]),0);
 glVertex3f(transformX(t[18]),transformY(t[19]),0);
 glVertex3f(transformX(t[22]),transformY(t[23]),0);
 glEnd();
 
 //��������
 glBegin(GL_TRIANGLE_STRIP);
 glVertex3f(transformX(t[0]),transformY(t[1]),0);
 glVertex3f(transformX(t[2]),transformY(t[3]),0);
 glVertex3f(transformX(t[4]),transformY(t[5]),0);
 glVertex3f(transformX(t[6]),transformY(t[7]),0);
 glVertex3f(transformX(t[8]),transformY(t[9]),0);
 glEnd();

 //������
 glColor3f(0.8, 0.9 - 0.7*gad[koi].krv, 0.9 - 0.7*gad[koi].krv);
 glBegin(GL_TRIANGLE_STRIP);
 glVertex3f(transformX(t[36]),transformY(t[37]),0.01);
 glVertex3f(transformX(t[44]),transformY(t[45]),0.01);
 glVertex3f(transformX(t[46]),transformY(t[47]),0.01);
 glVertex3f(transformX(t[40]),transformY(t[41]),0.01);
 glVertex3f(transformX(t[42]),transformY(t[43]),0.01);
 glEnd();
 
 //����� ������
 glColor3f(0.8, 0.5, 0.5);
 glBegin(GL_TRIANGLES);
 glVertex3f(transformX(t[56]),transformY(t[57]),0.01);
 glVertex3f(transformX(t[58]),transformY(t[59]),0.01);
 glVertex3f(transformX(t[60]),transformY(t[61]),0.01);
 glEnd();
/*
 glColor3f(1,1,1);
 unsigned i=44;
 putpixel(t[i], t[i+1], 0.5,0);
*/

 glColor3f(0.66, 0.66, 0.66); 
 glBegin(GL_LINES);
 //��� 
 line(t[0],t[1],t[2],t[3],0.02,1); 
 line(t[0],t[1],t[4],t[5],0.02,1);
 
 //����
 line(t[2],t[3],t[6],t[7],0.02,1); 
 line(t[4],t[5],t[8],t[9],0.02,1);
 line(t[6],t[7],t[10],t[11],0.02,1);
 line(t[8],t[9],t[10],t[11],0.02,1);
 
 line(t[6],t[7],t[12],t[13],0.02,1);
 line(t[8],t[9],t[14],t[15],0.02,1);

 //�����
 line(t[12],t[13],t[16],t[17],0.02,1);
 line(t[18],t[19],t[14],t[15],0.02,1);
 line(t[16],t[17],t[20],t[21],0.02,1);
 line(t[18],t[19],t[22],t[23],0.02,1);
 //����� - ����� ����
 line(t[20],t[21],t[24],t[25],0.02,1);
 line(t[22],t[23],t[26],t[27],0.02,1);
 line(t[24],t[25],t[28],t[29],0.02,1);
 line(t[26],t[27],t[30],t[31],0.02,1);
 //������
 line(t[28],t[29],t[32],t[33],0.02,1);
 line(t[30],t[31],t[34],t[35],0.02,1);
 line(t[32],t[33],t[34],t[35],0.02,1);
 
 //���������
 glColor3f(0.7, 0.7, 0.7);
 //���
 line(t[36],t[37],t[38],t[39],0.02,1);
 line(t[40],t[41],t[42],t[43],0.02,1);
 line(t[40],t[41],t[44],t[45],0.02,1);
 line(t[46],t[47],t[42],t[43],0.02,1);

 line(t[36],t[37],t[46],t[47],0.02,1);
 line(t[36],t[37],t[44],t[45],0.02,1);
 line(t[38],t[39],t[44],t[45],0.02,1);
 line(t[38],t[39],t[46],t[47],0.02,1);
 
 //����� 
 line(t[48],t[49],t[52],t[53],0.02,1);
 line(t[50],t[51],t[54],t[55],0.02,1);
 line(t[58],t[59],t[54],t[55],0.02,1);
 line(t[56],t[57],t[52],t[53],0.02,1);
 
 //������
 glColor3f(0.3, 0.6, 0.9);
 line(t[56],t[57],t[58],t[59],0.02,1);
 line(t[56],t[57],t[60],t[61],0.02,1);
 line(t[58],t[59],t[60],t[61],0.02,1);
 
 glEnd();

}

void obraz2(int koi)
{
/*
 glColor3f( 0.5, 0.5, 0.5 );
 glDisable(GL_POINT_SMOOTH);
 glDisable(GL_LINE_SMOOTH);
 glDisable(GL_BLEND);
 glLineWidth(1);

 rectangle(gad[koi].koordinati.x-radius[1], gad[koi].koordinati.y-radius[1], gad[koi].koordinati.x+radius[1], gad[koi].koordinati.y+radius[1], -0.5 ,0);
 line(gad[koi].koordinati.x+radius[1],gad[koi].koordinati.y,gad[koi].koordinati.x-radius[1],gad[koi].koordinati.y,-0.5,0);
 line(gad[koi].koordinati.x,gad[koi].koordinati.y+radius[1],gad[koi].koordinati.x,gad[koi].koordinati.y-radius[1],-0.5,0);
 circle(gad[koi].koordinati.x, gad[koi].koordinati.y, 0, radius[1], 0);
*/
 double t[40];
 
 //line(gad[koi].koordinati.x, gad[koi].koordinati.y,
  //    radius*cos(gad[koi].koordinati.posoka)+gad[koi].koordinati.x, radius*sin(gad[koi].koordinati.posoka)+gad[koi].koordinati.y,0.5);
 /*glColor3f(1,1,1);
 OutTextXY(s,1,gad[koi].koordinati.x, gad[koi].koordinati.y, 0.9);*/
  
 //���  
 t[0]=gad[koi].koordinati.x + (razmer[1]/20.0)*cos(gad[koi].koordinati.posoka);
 t[1]=gad[koi].koordinati.y + (razmer[1]/20.0)*sin(-gad[koi].koordinati.posoka);
 t[2]=gad[koi].koordinati.x + (razmer[1]/40.0)*cos(gad[koi].koordinati.posoka+M_PI*0.3);
 t[3]=gad[koi].koordinati.y + (razmer[1]/40.0)*sin(-gad[koi].koordinati.posoka-M_PI*0.3);
 t[4]=gad[koi].koordinati.x + (razmer[1]/40.0)*cos(gad[koi].koordinati.posoka+M_PI*1.7);
 t[5]=gad[koi].koordinati.y + (razmer[1]/40.0)*sin(-gad[koi].koordinati.posoka-M_PI*1.7);
 //������ ����
 t[6]=gad[koi].koordinati.x + (razmer[1]/23.0)*cos(gad[koi].koordinati.posoka+M_PI*0.55);
 t[7]=gad[koi].koordinati.y + (razmer[1]/23.0)*sin(-gad[koi].koordinati.posoka-M_PI*0.55);
 t[8]=gad[koi].koordinati.x + (razmer[1]/23.0)*cos(gad[koi].koordinati.posoka+M_PI*1.45);
 t[9]=gad[koi].koordinati.y + (razmer[1]/23.0)*sin(-gad[koi].koordinati.posoka-M_PI*1.45);
 //����� ����
 t[10]=gad[koi].koordinati.x + (razmer[1]/20.0)*cos(gad[koi].koordinati.posoka+M_PI*0.7);
 t[11]=gad[koi].koordinati.y + (razmer[1]/20.0)*sin(-gad[koi].koordinati.posoka-M_PI*0.7);
 t[12]=gad[koi].koordinati.x + (razmer[1]/20.0)*cos(gad[koi].koordinati.posoka+M_PI*1.3);
 t[13]=gad[koi].koordinati.y + (razmer[1]/20.0)*sin(-gad[koi].koordinati.posoka-M_PI*1.3);
 //������ - ����� ����
 t[14]=gad[koi].koordinati.x + (razmer[1]/19.0)*cos(gad[koi].koordinati.posoka+M_PI*0.87);
 t[15]=gad[koi].koordinati.y + (razmer[1]/19.0)*sin(-gad[koi].koordinati.posoka-M_PI*0.87);
 t[16]=gad[koi].koordinati.x + (razmer[1]/19.0)*cos(gad[koi].koordinati.posoka+M_PI*1.13);
 t[17]=gad[koi].koordinati.y + (razmer[1]/19.0)*sin(-gad[koi].koordinati.posoka-M_PI*1.13);
//������ - ����� ����
 t[18]=gad[koi].koordinati.x + (razmer[1]/18.0)*cos(gad[koi].koordinati.posoka+M_PI*0.91);
 t[19]=gad[koi].koordinati.y + (razmer[1]/18.0)*sin(-gad[koi].koordinati.posoka-M_PI*0.91);
 t[20]=gad[koi].koordinati.x + (razmer[1]/18.0)*cos(gad[koi].koordinati.posoka+M_PI*1.09);
 t[21]=gad[koi].koordinati.y + (razmer[1]/18.0)*sin(-gad[koi].koordinati.posoka-M_PI*1.09);
 
 //-- ��������� --//
 //���
 t[22]=gad[koi].koordinati.x + (razmer[1]/23.0)*cos(gad[koi].koordinati.posoka);
 t[23]=gad[koi].koordinati.y + (razmer[1]/23.0)*sin(-gad[koi].koordinati.posoka);
 t[24]=gad[koi].koordinati.x + (razmer[1]/55.0)*cos(gad[koi].koordinati.posoka);
 t[25]=gad[koi].koordinati.y + (razmer[1]/55.0)*sin(-gad[koi].koordinati.posoka);
 //������ - �����
 t[26]=gad[koi].koordinati.x + (razmer[1]/60.0)*cos(gad[koi].koordinati.posoka+M_PI*0.5);
 t[27]=gad[koi].koordinati.y + (razmer[1]/60.0)*sin(-gad[koi].koordinati.posoka-M_PI*0.5);
 t[28]=gad[koi].koordinati.x + (razmer[1]/60.0)*cos(gad[koi].koordinati.posoka+M_PI*1.5);
 t[29]=gad[koi].koordinati.y + (razmer[1]/60.0)*sin(-gad[koi].koordinati.posoka-M_PI*1.5);
 //������ - �����
 t[30]=gad[koi].koordinati.x + (razmer[1]/34.0)*cos(gad[koi].koordinati.posoka+M_PI*0.83);
 t[31]=gad[koi].koordinati.y + (razmer[1]/34.0)*sin(-gad[koi].koordinati.posoka-M_PI*0.83);
 t[32]=gad[koi].koordinati.x + (razmer[1]/34.0)*cos(gad[koi].koordinati.posoka+M_PI*1.17);
 t[33]=gad[koi].koordinati.y + (razmer[1]/34.0)*sin(-gad[koi].koordinati.posoka-M_PI*1.17);
 //������ - ���
 t[34]=gad[koi].koordinati.x + (razmer[1]/150.0)*cos(gad[koi].koordinati.posoka+M_PI);
 t[35]=gad[koi].koordinati.y + (razmer[1]/150.0)*sin(-gad[koi].koordinati.posoka+M_PI);
 //������
 t[36]=gad[koi].koordinati.x + (razmer[1]/24.0)*cos(gad[koi].koordinati.posoka+M_PI*0.15);
 t[37]=gad[koi].koordinati.y + (razmer[1]/24.0)*sin(-gad[koi].koordinati.posoka-M_PI*0.15);
 t[38]=gad[koi].koordinati.x + (razmer[1]/24.0)*cos(gad[koi].koordinati.posoka+M_PI*1.85);
 t[39]=gad[koi].koordinati.y + (razmer[1]/24.0)*sin(-gad[koi].koordinati.posoka-M_PI*1.85);

 //������
 glColor3f(0, 0.4, 0.8);
 glBegin(GL_TRIANGLE_STRIP);
 glVertex3f(transformX(t[0]), transformY(t[1]), 0);
 glVertex3f(transformX(t[2]), transformY(t[3]), 0);  
 glVertex3f(transformX(t[4]), transformY(t[5]), 0);
 glVertex3f(transformX(t[6]), transformY(t[7]), 0);
 glVertex3f(transformX(t[8]), transformY(t[9]), 0);
 glVertex3f(transformX(t[10]), transformY(t[11]), 0); 
 glVertex3f(transformX(t[12]), transformY(t[13]), 0);
 glVertex3f(transformX(t[14]), transformY(t[15]), 0);
 glVertex3f(transformX(t[16]), transformY(t[17]), 0);
 glEnd();

 glColor3f(0.85, 0.7 - (0.2 - 0.2*gad[koi].krv), 0);
 glBegin(GL_TRIANGLE_STRIP);
 glVertex3f(transformX(t[24]), transformY(t[25]), 0.01);
 glVertex3d(transformX(t[26]), transformY(t[27]), 0.01);
 glVertex3d(transformX(t[28]), transformY(t[29]), 0.01);
 glVertex3d(transformX(t[30]), transformY(t[31]), 0.01);
 glVertex3d(transformX(t[32]), transformY(t[33]), 0.01);
 glEnd();

//������� 
 glColor3f(0.2, 0.6, 0.9); 
 glBegin(GL_LINES); 
 
 //���  
 line(t[0],t[1],t[2],t[3],0.01,1); 
 line(t[0],t[1],t[4],t[5],0.01,1);

 //������ ����
 line(t[6],t[7],t[2],t[3],0.01,1);
 line(t[4],t[5],t[8],t[9],0.01,1);
 
 //����� ����
 line(t[6],t[7],t[10],t[11],0.01,1);
 line(t[12],t[13],t[8],t[9],0.01,1);
 
 //������ - ����� ����
 line(t[14],t[15],t[10],t[11],0.01,1);
 line(t[12],t[13],t[16],t[17],0.01,1);
 line(t[14],t[15],t[16],t[17],0.01,1);

 //������ - ����� ����
 line(t[14],t[15],t[18],t[19],0.02,1);
 line(t[20],t[21],t[16],t[17],0.02,1);
 line(t[18],t[19],t[20],t[21],0.02,1);
 
//��������� -----------------------------------
 //���
 line(t[22],t[23],t[24],t[25],0.02,1);
 //����� - �����
 line(t[26],t[27],t[6],t[7],0.02,1);
 line(t[28],t[29],t[8],t[9],0.02,1);
 //����� - �����
 line(t[10],t[11],t[30],t[31],0.02,1);
 line(t[12],t[13],t[32],t[33],0.02,1);
 //����� - �����
 line(t[30],t[31],t[14],t[15],0.02,1);
 line(t[32],t[33],t[16],t[17],0.02,1);
 //������
 line(t[2],t[3],t[36],t[37],0.02,1);
 line(t[4],t[5],t[38],t[39],0.02,1);
  
 glColor3f(1.0, 0.8, 0.5);
 //������ - �����
 line(t[24],t[25],t[26],t[27],0.02,1);
 line(t[24],t[25],t[28],t[29],0.02,1);
  
 //������ - �����
 line(t[26],t[27],t[30],t[31],0.02,1);
 line(t[28],t[29],t[32],t[33],0.02,1);
 line(t[30],t[31],t[32],t[33],0.02,1);
 
 //������ - ���
 line(t[34],t[35],t[30],t[31],0.02,1);
 line(t[24],t[25],t[34],t[35],0.02,1);
 line(t[34],t[35],t[32],t[33],0.02,1);
 
 glEnd();
 /*glColor3f(1,1,1);
 unsigned i=36;
 putpixel(t[i], t[i+1], 0.5); //42, 43*/
 
}

void obraz1(int koi)
{
/*
 glColor3f( 0.5, 0.5, 0.5 );
 glDisable(GL_POINT_SMOOTH);
 glDisable(GL_LINE_SMOOTH);
 glDisable(GL_BLEND);
 glLineWidth(1);

 rectangle(gad[koi].koordinati.x-radius[0], gad[koi].koordinati.y-radius[0], gad[koi].koordinati.x+radius[0], gad[koi].koordinati.y+radius[0], -0.5 ,0);
 line(gad[koi].koordinati.x+radius[0],gad[koi].koordinati.y,gad[koi].koordinati.x-radius[0],gad[koi].koordinati.y,-0.5,0);
 line(gad[koi].koordinati.x,gad[koi].koordinati.y+radius[0],gad[koi].koordinati.x,gad[koi].koordinati.y-radius[0],-0.5,0);
 circle(gad[koi].koordinati.x, gad[koi].koordinati.y, 0, radius[0], 0);
 */ 
 double t[60];
 
 //line(gad[koi].koordinati.x, gad[koi].koordinati.y,
  //    radius*cos(gad[koi].koordinati.posoka)+gad[koi].koordinati.x, radius*sin(gad[koi].koordinati.posoka)+gad[koi].koordinati.y,0.5);
 /*char s[256];
 sprintf(s,"%d",koi);
 
 glColor3f(1,1,1);
 OutTextXY(s,1,gad[koi].koordinati.x, gad[koi].koordinati.y, 0.9);*/
   
 //����� ����
 t[0]=gad[koi].koordinati.x + (razmer[0]/34.0)*cos(gad[koi].koordinati.posoka);
 t[1]=gad[koi].koordinati.y + (razmer[0]/34.0)*sin(-gad[koi].koordinati.posoka);
 t[2]=gad[koi].koordinati.x + (razmer[0]/50.0)*cos(gad[koi].koordinati.posoka+M_PI*0.625);
 t[3]=gad[koi].koordinati.y + (razmer[0]/50.0)*sin(-gad[koi].koordinati.posoka-M_PI*0.625);
 t[4]=gad[koi].koordinati.x + (razmer[0]/50.0)*cos(gad[koi].koordinati.posoka+M_PI*1.375);
 t[5]=gad[koi].koordinati.y + (razmer[0]/50.0)*sin(-gad[koi].koordinati.posoka-M_PI*1.375);
 
 //����� ����
 t[6]=gad[koi].koordinati.x + (razmer[0]/40.0)*cos(gad[koi].koordinati.posoka+M_PI*0.85);
 t[7]=gad[koi].koordinati.y + (razmer[0]/40.0)*sin(-gad[koi].koordinati.posoka-M_PI*0.85);
 t[8]=gad[koi].koordinati.x + (razmer[0]/40.0)*cos(gad[koi].koordinati.posoka+M_PI*1.15);
 t[9]=gad[koi].koordinati.y + (razmer[0]/40.0)*sin(-gad[koi].koordinati.posoka-M_PI*1.15);
 
 //������
 t[10]=gad[koi].koordinati.x + (razmer[0]/25.0)*cos(gad[koi].koordinati.posoka+M_PI*0.91);
 t[11]=gad[koi].koordinati.y + (razmer[0]/25.0)*sin(-gad[koi].koordinati.posoka-M_PI*0.91);
 t[12]=gad[koi].koordinati.x + (razmer[0]/25.0)*cos(gad[koi].koordinati.posoka+M_PI*1.09);
 t[13]=gad[koi].koordinati.y + (razmer[0]/25.0)*sin(-gad[koi].koordinati.posoka-M_PI*1.09);
 
 //���� �����
 t[14]=gad[koi].koordinati.x + (razmer[0]/75.0)*cos(gad[koi].koordinati.posoka+M_PI*0.5);
 t[15]=gad[koi].koordinati.y + (razmer[0]/75.0)*sin(-gad[koi].koordinati.posoka-M_PI*0.5);
 t[16]=gad[koi].koordinati.x + (razmer[0]/18.75)*cos(gad[koi].koordinati.posoka+M_PI*0.2);
 t[17]=gad[koi].koordinati.y + (razmer[0]/18.75)*sin(-gad[koi].koordinati.posoka-M_PI*0.2);
 
 t[18]=gad[koi].koordinati.x + (razmer[0]/20.7)*cos(gad[koi].koordinati.posoka+M_PI*0.6);
 t[19]=gad[koi].koordinati.y + (razmer[0]/20.7)*sin(-gad[koi].koordinati.posoka-M_PI*0.6);
 t[20]=gad[koi].koordinati.x + (razmer[0]/18.2)*cos(gad[koi].koordinati.posoka+M_PI*0.84);
 t[21]=gad[koi].koordinati.y + (razmer[0]/18.2)*sin(-gad[koi].koordinati.posoka-M_PI*0.84); 
 
 t[22]=gad[koi].koordinati.x + (razmer[0]/30.0)*cos(gad[koi].koordinati.posoka+M_PI*0.89);
 t[23]=gad[koi].koordinati.y + (razmer[0]/30.0)*sin(-gad[koi].koordinati.posoka-M_PI*0.89);
 
 //��������� - ����
 t[24]=gad[koi].koordinati.x + (razmer[0]/26.1)*cos(gad[koi].koordinati.posoka+M_PI*0.62);
 t[25]=gad[koi].koordinati.y + (razmer[0]/26.1)*sin(-gad[koi].koordinati.posoka-M_PI*0.62);
 t[26]=gad[koi].koordinati.x + (razmer[0]/24.0)*cos(gad[koi].koordinati.posoka+M_PI*0.79);
 t[27]=gad[koi].koordinati.y + (razmer[0]/24.0)*sin(-gad[koi].koordinati.posoka-M_PI*0.79);

 t[28]=gad[koi].koordinati.x + (razmer[0]/27.3)*cos(gad[koi].koordinati.posoka+M_PI*0.31);
 t[29]=gad[koi].koordinati.y + (razmer[0]/27.3)*sin(-gad[koi].koordinati.posoka-M_PI*0.31);
 t[30]=gad[koi].koordinati.x + (razmer[0]/50.0)*cos(gad[koi].koordinati.posoka+M_PI*0.55);
 t[31]=gad[koi].koordinati.y + (razmer[0]/50.0)*sin(-gad[koi].koordinati.posoka-M_PI*0.55);
 
 t[32]=gad[koi].koordinati.x + (razmer[0]/30.0)*cos(gad[koi].koordinati.posoka+M_PI*0.5);
 t[33]=gad[koi].koordinati.y + (razmer[0]/30.0)*sin(-gad[koi].koordinati.posoka-M_PI*0.5);
 t[34]=gad[koi].koordinati.x + (razmer[0]/33.3)*cos(gad[koi].koordinati.posoka+M_PI*0.75);
 t[35]=gad[koi].koordinati.y + (razmer[0]/33.3)*sin(-gad[koi].koordinati.posoka-M_PI*0.75);

 //����� �����
 t[36]=gad[koi].koordinati.x + (razmer[0]/75.0)*cos(gad[koi].koordinati.posoka+M_PI*1.5);
 t[37]=gad[koi].koordinati.y + (razmer[0]/75.0)*sin(-gad[koi].koordinati.posoka-M_PI*1.5);
 t[38]=gad[koi].koordinati.x + (razmer[0]/18.75)*cos(gad[koi].koordinati.posoka+M_PI*1.8);
 t[39]=gad[koi].koordinati.y + (razmer[0]/18.75)*sin(-gad[koi].koordinati.posoka-M_PI*1.8);

 t[40]=gad[koi].koordinati.x + (razmer[0]/20.7)*cos(gad[koi].koordinati.posoka+M_PI*1.4);
 t[41]=gad[koi].koordinati.y + (razmer[0]/20.7)*sin(-gad[koi].koordinati.posoka-M_PI*1.4);
 t[42]=gad[koi].koordinati.x + (razmer[0]/18.2)*cos(gad[koi].koordinati.posoka+M_PI*1.16);
 t[43]=gad[koi].koordinati.y + (razmer[0]/18.2)*sin(-gad[koi].koordinati.posoka-M_PI*1.16);

 t[44]=gad[koi].koordinati.x + (razmer[0]/30.0)*cos(gad[koi].koordinati.posoka+M_PI*1.11);
 t[45]=gad[koi].koordinati.y + (razmer[0]/30.0)*sin(-gad[koi].koordinati.posoka-M_PI*1.11);

 //��������� - �����
 t[46]=gad[koi].koordinati.x + (razmer[0]/26.1)*cos(gad[koi].koordinati.posoka+M_PI*1.38);
 t[47]=gad[koi].koordinati.y + (razmer[0]/26.1)*sin(-gad[koi].koordinati.posoka-M_PI*1.38);
 t[48]=gad[koi].koordinati.x + (razmer[0]/24.0)*cos(gad[koi].koordinati.posoka+M_PI*1.21);
 t[49]=gad[koi].koordinati.y + (razmer[0]/24.0)*sin(-gad[koi].koordinati.posoka-M_PI*1.21);

 t[50]=gad[koi].koordinati.x + (razmer[0]/27.3)*cos(gad[koi].koordinati.posoka+M_PI*1.69);
 t[51]=gad[koi].koordinati.y + (razmer[0]/27.3)*sin(-gad[koi].koordinati.posoka-M_PI*1.69);
 t[52]=gad[koi].koordinati.x + (razmer[0]/50.0)*cos(gad[koi].koordinati.posoka+M_PI*1.45);
 t[53]=gad[koi].koordinati.y + (razmer[0]/50.0)*sin(-gad[koi].koordinati.posoka-M_PI*1.45);
 
 t[54]=gad[koi].koordinati.x + (razmer[0]/30.0)*cos(gad[koi].koordinati.posoka+M_PI*1.5);
 t[55]=gad[koi].koordinati.y + (razmer[0]/30.0)*sin(-gad[koi].koordinati.posoka-M_PI*1.5);
 t[56]=gad[koi].koordinati.x + (razmer[0]/33.3)*cos(gad[koi].koordinati.posoka+M_PI*1.25);
 t[57]=gad[koi].koordinati.y + (razmer[0]/33.3)*sin(-gad[koi].koordinati.posoka-M_PI*1.25);

 //������
 t[58]=gad[koi].koordinati.x - (razmer[0]/150.0)*cos(gad[koi].koordinati.posoka);
 t[59]=gad[koi].koordinati.y - (razmer[0]/150.0)*sin(-gad[koi].koordinati.posoka);

 //����� ����
 glColor3f( 0.5*(1-gad[koi].krv), 0.66, (1-gad[koi].krv)*0.5);
 glBegin(GL_LINES);
 line(t[0],t[1],t[2],t[3],0.01,1);
 line(t[0],t[1],t[4],t[5],0.01,1);
 //����� ����
 line(t[6],t[7],t[2],t[3],0.01,1);
 line(t[4],t[5],t[8],t[9],0.01,1);
 //������
 line(t[6],t[7],t[10],t[11],0.01,1);
 line(t[12],t[13],t[8],t[9],0.01,1);
 line(t[10],t[11],t[12],t[13],0.01,1);
 //���� �����
 glColor3f(1, 0.33, 0.15);
 line(t[14],t[15],t[16],t[17],0.01,1); 
 line(t[18],t[19],t[16],t[17],0.01,1);
 line(t[18],t[19],t[20],t[21],0.01,1);
 line(t[22],t[23],t[20],t[21],0.01,1); 
 //����� �����
 line(t[36],t[37],t[38],t[39],0.01,1);
 line(t[40],t[41],t[38],t[39],0.01,1);
 line(t[40],t[41],t[42],t[43],0.01,1);
 line(t[44],t[45],t[42],t[43],0.01,1);
 glEnd();
 
 //���� - ������
 glColor3f( 0.5*(1-gad[koi].krv), 0.66, (1-gad[koi].krv)*0.5);
 glBegin(GL_TRIANGLE_STRIP); 
 glVertex3f(transformX(t[0]), transformY(t[1]), 0.01);
 glVertex3f(transformX(t[2]), transformY(t[3]), 0.01);
 glVertex3f(transformX(t[4]), transformY(t[5]), 0.01);
 glVertex3f(transformX(t[6]), transformY(t[7]), 0.01);
 glVertex3f(transformX(t[8]), transformY(t[9]), 0.01);
 glVertex3f(transformX(t[10]), transformY(t[11]), 0.01);
 glVertex3f(transformX(t[12]), transformY(t[13]), 0.01); 
 
 glColor3f(1, 0.33, 0.15);
 //���� �����
 glVertex3f(transformX(t[14]), transformY(t[15]), 0.01);
 glVertex3f(transformX(t[16]), transformY(t[17]), 0.01);
 glVertex3f(transformX(t[18]), transformY(t[19]), 0.01);
 glVertex3f(transformX(t[22]), transformY(t[23]), 0.01);
 glVertex3f(transformX(t[20]), transformY(t[21]), 0.01);
 glEnd();
 //����� ����� 
 glBegin(GL_TRIANGLE_STRIP);
 glVertex3f(transformX(t[36]), transformY(t[37]), 0.01);
 glVertex3f(transformX(t[38]), transformY(t[39]), 0.01);
 glVertex3f(transformX(t[40]), transformY(t[41]), 0.01); 
 glVertex3f(transformX(t[44]), transformY(t[45]), 0.01);
 glVertex3f(transformX(t[42]), transformY(t[43]), 0.01);
 glEnd();

 glColor3f(1, 1, 0);
 //��������� - ����
 glBegin(GL_LINES);
 line(t[24],t[25],t[26],t[27],0.02,1);
 line(t[28],t[29],t[30],t[31],0.02,1);
 line(t[30],t[31],t[24],t[25],0.02,1);
 line(t[32],t[33],t[34],t[35],0.02,1);
 //��������� - �����
 line(t[46],t[47],t[48],t[49],0.02,1); 
 line(t[50],t[51],t[52],t[53],0.02,1);
 line(t[52],t[53],t[46],t[47],0.02,1);
 line(t[54],t[55],t[56],t[57],0.02,1);
 glEnd();
 
 //������
 glColor3f(1, 1, 0.5);
 glPushMatrix();
 glTranslatef(transformX(t[58]), transformY(t[59]), 0.02);
 gluDisk(quad, transformX(razmer[0] * 0.336), transformX(razmer[0] * 0.339), 8, 1);
 glPopMatrix();
}

void smrt(ship::coords *koord, unsigned int koi_gad, ship::coords *vzrivove, unsigned maxBullets)
{
     for (j=0; j<maxBullets; j++)
         {
			 if (koord[j].posoka==200) continue;
			 
             double d1=razstoqnie(koord[j].x, koord[j].y, gad[koi_gad].koordinati.x, gad[koi_gad].koordinati.y);
			 			         
			 if (d1 <= radius[gad[koi_gad].tip]*radius[gad[koi_gad].tip])
                {
				 gad[koi_gad].krv-=izkarvane_na_krv[gad[koi_gad].tip];
		        
		         if (gad[koi_gad].krv > 0)
		              for (i=1; i<10; i++)
		                    if (!vzrivove[i].posoka)
		                       {
		                         vzrivove[i].posoka = koord[j].posoka - M_2PI;
                                 vzrivove[i].x = koord[j].x;
                                 vzrivove[i].y = koord[j].y;
                                 break;
		                       }


		            koord[j].posoka=200;
                }
               
               if (gad[koi_gad].krv<=0 && gad[koi_gad].krv > -10)
                  {
                  jivi--;
                  toczki += gad[koi_gad].toczki;
                  
                  if  (gad[koi_gad].tip < 3) ubit.posoka=1;
                  else                       ubit.posoka=2;
                  if  (gad[koi_gad].tip == 1) ubit.posoka=1+random(2);
                  
                  ubit.x = gad[koi_gad].koordinati.x;
                  ubit.y = gad[koi_gad].koordinati.y;
                  
                  gad[koi_gad].krv=-10;
				  vzrivove[0].posoka = koord[j].posoka;
                  vzrivove[0].x = gad[koi_gad].koordinati.x;
                  vzrivove[0].y = gad[koi_gad].koordinati.y;
                  koord[j].posoka=200;
                  }
          }
 }    

void butane(ship::coords *korab, double R)
{
const double _radius=maxy/400.0;
double X,Y,d;

 for (i=0; i<max; i++)
     {
     if (gad[i].krv <= 0) continue;
     
            d = razstoqnie(gad[i].koordinati.x, gad[i].koordinati.y,
                korab->x, korab->y);

          if (d < (R + radius[gad[i].tip])*(R + radius[gad[i].tip]))
             {             
                Y=_radius*sin(-korab->posoka);
                X=_radius*cos(-korab->posoka);
                
                if (kop.w)
                   {
                     korab->x+=X;
					 korab->y+=Y;               
					 if (razstoqnie(gad[i].koordinati.x, gad[i].koordinati.y, korab->x, korab->y) < d)                  
                         kop.w=0;
					 korab->y-=Y;
					 korab->x-=X;
					}
             }
     }
}

void butane_zlo(double radius_korab)
{
  const double _radius=maxy/200.0;
  double X, Y, d, radius_tekuszt;
  
  for (i=0;i<max;i++)
      {
      if (gad[i].krv <= 0) continue;
      if (gad[i].stop) gad[i].stop-=1;
      
      for (j=0;j<=max;j++)
          {
            if (i==j || gad[j].krv <= 0) continue;
                          
            d=razstoqnie(gad[i].koordinati.x, gad[i].koordinati.y, gad[j].koordinati.x, gad[j].koordinati.y);

			radius_tekuszt = (j == max) ? radius_korab : radius[gad[j].tip];
            
            if (d < (radius[gad[i].tip] + radius_tekuszt) * (radius[gad[i].tip] + radius_tekuszt))
               {
                 Y=_radius*sin(-gad[i].koordinati.posoka);
                 X=_radius*cos(-gad[i].koordinati.posoka);

                 gad[i].koordinati.x+=X;
                 gad[i].koordinati.y+=Y;
                                            
                 if (razstoqnie(gad[i].koordinati.x, gad[i].koordinati.y, gad[j].koordinati.x, gad[j].koordinati.y) < d)
                    gad[i].stop+=1;
                                     
                 gad[i].koordinati.y-=Y;
                 gad[i].koordinati.x-=X;
               }
          }
      }
}

void entelekt(double Cdelay, ship::coords *nova_strelba)
{
	if (this->bez_entelekt)
	{
		for (i=0; this->bez_entelekt && i<max; i++)
		{
			gad[i].vreme.strelba_nova = Cdelay;
			gad[i].vreme.napred = Cdelay;
			gad[i].vreme.zavoi = Cdelay;
		}
		return;
	}
	
	
	for (i=0;i<max;i++)
	{
		if (gad[i].krv <= 0) continue;

		// �������
		if (Cdelay - gad[i].vreme.strelba_nova >= gad[i].timeout_nova_strelba)
		{
			float stpka = radius[gad[i].tip] * (Cdelay - gad[i].vreme.strelba_nova) / gad[i].timeout_nova_strelba;

			nova_strelba->x = gad[i].koordinati.x + stpka * cos(-gad[i].koordinati.posoka);
			nova_strelba->y = gad[i].koordinati.y + stpka * sin(-gad[i].koordinati.posoka);
			nova_strelba->posoka = -gad[i].koordinati.posoka;
			
			zvuk.timeout.kurszum_zlo = -2;
			
			gad[i].timeout_nova_strelba = random(this->timeout_strelba);
			gad[i].vreme.strelba_nova = Cdelay;
		}

		// ������ ������
		if (Cdelay - gad[i].vreme.napred >= this->timeout_napred[gad[i].tip])
		{				
			if (!gad[i].stop)
			{
				float stpka = gad[i].stpka_napred * (Cdelay - gad[i].vreme.napred) / this->timeout_napred[gad[i].tip];	

				gad[i].koordinati.y += stpka * sin(-gad[i].koordinati.posoka);
				gad[i].koordinati.x += stpka * cos(-gad[i].koordinati.posoka);

				if (gad[i].koordinati.x>=maxx) gad[i].koordinati.x=2;
				if (gad[i].koordinati.y>=maxy) gad[i].koordinati.y=2;
				if (gad[i].koordinati.x<=1) gad[i].koordinati.x=maxx;
				if (gad[i].koordinati.y<=1) gad[i].koordinati.y=maxy;
			}

			gad[i].vreme.napred = Cdelay;
		}

		// ����� �� �����
		if ((Cdelay - gad[i].vreme.smqna_na_posoka >= this->timeout_smqna_na_posoka[gad[i].tip]) && (gad[i].smqna_na_posoka <= 0))
		{
			gad[i].vreme.smqna_na_posoka = Cdelay;
			gad[i].vreme.zavoi = Cdelay;
			gad[i].smqna_lqvo = random(3);

			if (!gad[i].stop)
			{
				if (gad[i].smqna_lqvo==2)
					gad[i].smqna_na_posoka = random(314159)/100000.0;
				else
				   gad[i].smqna_na_posoka = random(785398)/1000000.0;
			}
			else
			{
				gad[i].smqna_na_posoka = M_PI2+random(785398)/100000.0;
				gad[i].smqna_lqvo=random(2);
			}

		}

		// �����
		// ��� ������ �� �� ����� ���� � ���� � �� ���� �������,
		// �� �� ���� �� �� ���������
		if (Cdelay - gad[i].vreme.zavoi >= this->timeout_zavoi[gad[i].tip])
		{
			if (gad[i].smqna_na_posoka > 0)
			{
				float stpka = gad[i].stpka_zavoi * (Cdelay - gad[i].vreme.zavoi) / this->timeout_zavoi[gad[i].tip];
				
				stpka = gad[i].stop ? stpka * 1.5 : stpka;

				gad[i].smqna_na_posoka -= stpka;
				if (gad[i].smqna_lqvo==1)
					gad[i].koordinati.posoka += stpka;
				else if (!gad[i].smqna_lqvo)
					gad[i].koordinati.posoka -= stpka;

				if (gad[i].koordinati.posoka > M_2PI)
					gad[i].koordinati.posoka = 0;

				if (gad[i].koordinati.posoka < 0)
					gad[i].koordinati.posoka = M_2PI;
					
				gad[i].vreme.zavoi = Cdelay;
			}
			
		}
	}

}

void kontrol(ship::coords korab, double radius_korab, ship::coords *koord, ship::coords *vzrivove, int maxBullets)
{
unsigned int i;

  gad[max].koordinati.x=korab.x;
  gad[max].koordinati.y=korab.y;
  gad[max].koordinati.posoka=korab.posoka;
  
  butane_zlo(radius_korab);
  for (i=0; i<max; i++) if (gad[i].krv>0) smrt(koord, i, vzrivove, maxBullets);
                        else continue;
  
  //if (kop.a) gad[4].koordinati.posoka-=0.03;
  //if (kop.d) gad[4].koordinati.posoka+=0.03;
}

};